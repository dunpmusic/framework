<?php

require_once __DIR__ . '/../core/autoloader.php';
dunp_register_autoloader();

set_time_limit(1);
ini_set("max_execution_time", 1);
$core = new CoreLoader();
try {
    register_shutdown_function( array($core,"fatal_handler" ));
    set_error_handler(array($core, "errorHandler"), E_ERROR);
    
    $core->init();
    
    // Add basic web component
    $web = new \Dunp\Controller\WebComponent($core->getApp(), \Dunp\Component::COMPONENT_LOW_PRIORITY); // Low priority
    $web->addScheme(new Dunp\Scheme());
    $core->addComponent($web);
    
    $core->run("http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
    
} catch (Exception $ex) {
    $core->exception($ex);
}