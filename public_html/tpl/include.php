<?php

function get_templates_current_dir()
{
    return __DIR__;
}

function get_templates($path = array(__DIR__)) {
    $str = "
        {{=<% %>=}}";
    foreach ($path as $dir) {
        if(is_dir($dir))
        {
            $files = scandir($dir);
            foreach ($files as $value) {
                if ($value == "." || $value == "..")
                    continue;
                $pos = strpos($value, ".");
                if ($pos !== FALSE && substr($value, $pos) == ".html") {
                    $name = substr($value, 0, $pos);
                    $str .= "<template id=\"template_" . $name . "\">" . file_get_contents($dir . "/" . $value) . "</template>\n";
                }
            }
        }
    }
    return $str . "\n
        <%={{ }}=%>";
}
