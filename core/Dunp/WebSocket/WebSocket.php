<?php
/**
 * Created by PhpStorm.
 * User: manus
 * Date: 14/6/15
 * Time: 11:30
 */

namespace Dunp\WebSocket;


use Devristo\Phpws\Server\UriHandler\WebSocketUriHandler;

abstract class WebSocket extends WebSocketUriHandler {


    private $app;

    function __construct($app)
    {
        parent::__construct(null);
        $this->app = $app;
    }

    /**
     * @return mixed
     */
    public function getApp()
    {
        return $this->app;
    }



    public abstract function run();
} 