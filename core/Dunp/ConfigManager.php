<?php
namespace Dunp;

class ConfigManager extends Manager
{
    /**
     *
     * @var \Dunp\WebApp
     */
    var $app;
    public function __construct($app) {
        $this->app = $app;
    }
    
    public function getConfigurations()
    {
        $plugins = $this->app->getPluginManager()->getPlugins();
        $default = $this->getConfigFile();
        $config = array();
        $config = array_merge($config, $this->getDefaultConfig());
        foreach ($plugins as $plugin) {
            $config = array_merge($config, $plugin->getConfiguration());
            $pages = $plugin->getPages();
            foreach ($pages as $page) {
                $items = $page->getController()->getConfigurationsTemplate();
                $config = array_merge($config, $items);
            }
        }
        
        if(file_exists($default))
        {
            if($obj = json_decode(file_get_contents($default), true))
            {
                $config = array_merge($config, $obj);
            }
        }
        
        return $config;
    }

    public function get()
    {

        return (object) $this->getConfigurations();
    }
    
    public function getDefaultConfig()
    {
        return array(
            'upload.directory' => realpath(__DIR__ . "/../../dcu"), // Upload directory
            'database.host' => "localhost", // Database remote host
            'database.user' => "", // Database username
            'database.pass' => "", // Database password
            'database.name' => "dunp", // Database name
            'database.memcached' => false, // Memcached service enable/disable
            'database.ipp' => 25, // Default items pre page
            'site.configured' => false, // To enable web site set this to true
            'site.enabled' => true, // Subcription or login
            'site.debug' => true, // Debug mode
            'site.migrating' => true, // Debug mode
            'mail.smtp' => false, // Enable / disable SMTP for send. If it is disabled then mail function is used
            'scripts.minification' => false,
            'scripts.compression' => false,
            'scripts.remote' => false,
            'memcache' => array(
                'host' => "localhost",
                'port' => 11211
            ),
            'websocket' => array(
                'host' => "0.0.0.0",
                'port' => 8008
            )
        );
    }
    
    public function getConfigFile()
    {
        
        return realpath( __DIR__ . "/../../" ) . "/config.json";
    }

    public function isConfigCreated()
    {
        return file_exists($this->getConfigFile());
    }
    
    public function loadConfig()
    {
        global $config;
        $config = $this->getConfigurations();
        return true;
    }
    
    public function writeDefaultConfig($array) {
        return $this->writeConfig($this->getConfigFile(), $array);
    }

    public function writeConfig($filename, $array) {
//        $data = $this->generateConfig($array);
        return file_put_contents($filename, json_encode($array, 128));
    }

    public function load()
    {
        // TODO: Implement load() method.
        $this->loadConfig();
    }
}