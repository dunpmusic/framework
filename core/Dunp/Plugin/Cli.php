<?php
namespace Dunp\Plugin;

abstract class Cli
{
    private $app;
    public function __construct($app) {
        $this->app = $app;
    }
    abstract function run($argc, $argv);
    
    /**
     * 
     * @return \Dunp\WebApp
     */
    public function getApp()
    {
        return $this->app;
    }
}