<?php

namespace Dunp\Plugin;

class PluginRegister {

    private static $session = array(
        0 => array(
            'plugins' => array(),
            'plugins_dir_map' => array()
        )
    );
    private static $current = 0;

    public static function addPlugin($name) {
        $trace = debug_backtrace();
        PluginRegister::$session[PluginRegister::$current]['plugins'][] = $name;
        PluginRegister::$session[PluginRegister::$current]['plugins_dir_map'][$name] = basename(dirname($trace[0]["file"]));
    }

    /**
     * 
     * @return Plugin
     */
    public static function getPlugins() {
        return PluginRegister::$session[PluginRegister::$current]['plugins'];
    }

    public static function getPluginPath($plugin) {
        return PluginRegister::$session[PluginRegister::$current]['plugins_dir_map'][$plugin];
    }

    public static function setCurrent($current = 0) {
        PluginRegister::$current = $current;
        PluginRegister::$session[PluginRegister::$current] = array(
            'plugins' => array(),
            'plugins_dir_map' => array()
        );
    }

    public static function restart() {
        PluginRegister::$current = 0;
        PluginRegister::$session = array(
            0 => array(
                'plugins' => array(),
                'plugins_dir_map' => array()
            )
        );
    }

}
