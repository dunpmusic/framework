<?php

namespace Dunp\Plugin;

use Dunp\Manager;
use Dunp\WebApp;

class PluginManager extends Manager {

    /**
     *
     * @var Plugin
     */
    private $plugins = array();
    /**
     * @var Plugin
     */
    private $all_plugins = array();
    private $plugins_map = array();
    private $folder = "plugins";
    private $app;

    public function __construct($app, $folder = "plugins") {
        $this->folder = $folder;
        $this->app = $app;
    }

    /**
     * 
     * @return Plugin[]
     */
    public function getPlugins() {
        return $this->plugins;
    }

    public function getPage($page, & $plugin = null) {
        foreach ($this->plugins as $value) {
            if ($value->hasPage($page)) {
                $plugin = $value;
                return $value->getPage($page);
            }
        }
        return false;
    }

    /**
     * 
     * @param type $name
     * @return Plugin
     */
    public function getPlugin($name) {
        if ($this->hasPlugin($name))
            return $this->plugins_map[$name];
    }

    public function hasPlugin($name) {
        return isset($this->plugins_map[$name]);
    }

    public function reloadPlugins() {
        $this->plugins = array();
        $this->plugins_map = array();
        $this->loadPlugins($this->app);
    }

    public function getPluginsFolder() {
        return $this->folder;
    }

    /**
     * @param $app WebApp
     */
    private function loadPlugins($app) {
        global $config;
        try {
            foreach (scandir($this->folder) as $plugin) {
                if ($plugin != "." && $plugin != "..") {
                    if(is_dir($filename = $this->folder . "/$plugin"))
                    {
                        $filename = $this->folder . "/$plugin/plugin.php";
                        if (file_exists($filename)) {
                            include_once $filename;
                        }
                    }
                }
            }
            $list = PluginRegister::getPlugins();
            $plugins = array();
            $this->all_plugins = array();
            foreach ($list as $value) {

                $classname = "{$value}_plugin";
                if (class_exists($classname)) {
                    $plugin = new $classname($app, $this->folder . "/" . (PluginRegister::getPluginPath($value)));
                    $plugin->setNamespace($value);
                    $plugins[$plugin->getNamespace()] = $plugin;
                    /// Display only namespace for managing
                    $this->all_plugins[] = $plugin->getNamespace();
                }
            }
            $systemPlugins = $app->getDefinitionsManager()->get()->systemPlugins;
            if($app->getConfigManager()->get()->{"site.configured"})
            {

                $enabledPlugins = array();
                if(isset($config['plugins']))
                {
                    $enabledPlugins = array_keys( $config['plugins']);
                }
                $enabled = array_merge($systemPlugins, $enabledPlugins);
                $enabled[] = $app->getDefinitionsManager()->get()->defaultPlugin;
            }
            else{

                $enabled = $systemPlugins;
//                $enabled[] = $app->getDefinitionsManager()->get()->defaultPlugin;
            }
            foreach($enabled as $namespace)
            {
                if(isset($plugins[$namespace]))
                {
                    $plugin = $plugins[$namespace];
                    $value = get_class($plugin);
                    $plugin->autoloader();
                    $plugin->onRegisterManagers($app);
                    $plugin->loadPages();
                    $this->plugins_map[$value] = $plugin;
                    $this->plugins[] = $plugin;
                }
            }

        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }

    public function isValidPlugin($folder) {
        if (file_exists($folder . "/plugin.php")) {
            return true;
        }
        return false;
    }

    /**
     * 
     * @param type $path
     * @return Plugin
     */
    public function getPluginByPath($path) {
        if ($this->isValidPlugin($path)) {
            foreach (PluginRegister::getPlugins() as $value) {

                if (basename(PluginRegister::getPluginPath($value)) == basename($path) && basename($path) != "") {
                    return $this->getPlugin($value);
                }
            }
        }
    }

    /**
     * 
     * @param type $namespace
     * @return Plugin
     */
    public function getPluginByNamespace($namespace) {
        foreach (PluginRegister::getPlugins() as $value) {
            $plugin = $this->getPlugin($value);
            if ($plugin->getNamespace() == $namespace)
            {
                return $plugin;
            }
        }
        return null;
    }

    /**
     *
     * @param Plugin $plugin
     * @return type
     */
    public function getPluginPath($plugin) {
        return PluginRegister::getPluginPath($plugin->getNamespace());
    }


    /**
     * 
     * @param Plugin $plugin
     */
    public function getPluginIconAbsolutePath($plugin) {
        return realpath($this->folder . "/" . $this->getPluginPath($plugin) . "/" . $plugin->getIcon());
    }

    /**
     * 
     * @param Plugin $plugin
     */
    public function getPluginIconRelativePath($plugin, $relative = null) {
        if ($relative == null) {
            $relative = realpath($this->folder . "/../");
        }
        return str_replace($relative, "", $this->getPluginIconAbsolutePath($plugin));
    }

    public function readPluginLogo($namespace) {

        header("Content-Type: image/png");
        readfile($this->getPluginIconAbsolutePath($this->getPluginByNamespace($namespace)));
    }

    /**
     * @return Plugin
     */
    public function getAllPlugins()
    {
        return $this->all_plugins;
    }

    /**
     * @param Plugin $all_plugins
     */
    public function setAllPlugins($all_plugins)
    {
        $this->all_plugins = $all_plugins;
    }
    /**
     * @return Plugin
     */
    public function getPluginsDisabled()
    {
        return array_diff( $this->all_plugins , array_map(function($a){return $a->getNamespace();}, $this->getPlugins()) );
    }


    public function load()
    {
        // TODO: Implement load() method.
        $this->reloadPlugins();
    }
}
