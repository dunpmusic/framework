<?php

namespace Dunp\Plugin;

abstract class View extends \Dunp\View {

    // Retuns some like: plugins/<pname>/view/<pname>.html
    public function getViewFile() {
        if(file_exists($this->getTemplate()))
        {
            return $this->getTemplate();
        }

        return ($this->getBaseDir() . "/view/" . ( $this->getTemplate()==""?str_replace("\\", "/",  $this->getPageName()) : $this->getTemplate() ) . ".html");
    }
    
    public function getSectionPath($file) {
        return realpath($this->getBaseDir() . "/view/section/" . $file);
    }

}
