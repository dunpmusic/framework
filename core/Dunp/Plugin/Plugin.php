<?php

namespace Dunp\Plugin;

use Dunp\Cron\CronTask;
use Dunp\Functions;
use Dunp\WebApp;

class Plugin
{
    private $app;
    private $path;
    private $name;
    private $vendor;
    private $stable;
    private $description;
    private $icon;
    private $namespace;
    private $version;
    /**
     *
     * @var View 
     */
    private $pages = array();
    private $pages_map = array();
    private $pages_names = array();
    
    /**
     *
     * @var \Dunp\Component 
     */
    private $components = array();
    
    private $themes = array();
    private $requires = array();
    private $cliCommands = array();
    private $setupSteps = array();
    private $buildSteps = array();
    private $cronJobs = array();
    private $webSockets = array();


    /**
     * @param $app WebApp
     * @param $path
     * @param $name
     * @param string $vendor
     * @param string $icon
     * @param string $description
     * @param string $version
     */
    public function __construct($app, $path, $name, $vendor = "", $icon = "", $description = "", $version = "") {
        $this->app = $app;
        $this->path = $path;
        $this->name = $name;
        $this->vendor = $vendor;
        $this->icon = $icon;
        $this->description = $description;
        $this->version = $version;
    }
    
    public function setNamespace($namespace)
    {
        $this->namespace = $namespace;
    }
    
    public function getNamespace()
    {
        return str_replace("_", "-", $this->vendor . "/" . $this->namespace);
    }
    /**
     * 
     * @return View[]
     */
    public function getPages()
    {
        return $this->pages;
    }
    
    public function addPage($page)
    {
        $this->pages_names[] = $page;

    }


    public function loadPages()
    {
        foreach($this->pages_names as $page)
        {
            $filename = $this->path . "/page/$page.php";
            $classname = "{$page}_view";
            $classname = str_replace("/", "\\", $classname);
            if(file_exists($filename))
            {
                include_once $filename;
                if(!$this->loadPage($classname, $page))
                {
                    $classname = get_class($this) . "\\" . $classname;
                    $this->loadPage($classname, $page);
                }
            }


        }
    }

    private function loadPage($classname, $page)
    {
        if(class_exists($classname))
        {

            $reflexion = new \ReflectionClass($classname);
            $class = $reflexion->newInstanceArgs(array($this->app, $this->path));
            $this->pages_map[$page] = $class;
            $this->pages[] = $class;
            return true;
        }
        return false;
    }
    
    /**
     * 
     * @param \Dunp\Component $component
     */
    public function addComponent($component)
    {
        $this->components[] = $component;
    }
    /**
     * 
     * @return \Dunp\Component
     */
    public function getComponents()
    {
        return $this->components;
    }
    
    public function addTheme($theme)
    {
        $this->themes[] = $theme;
    }
    public function getThemes()
    {
        return $this->themes;
    }
    
    public function getName()
    {
        return $this->name;
    }
    public function getVendor()
    {
        return $this->vendor;
    }
    public function getDescription()
    {
        return $this->description;
    }
    
    public function getIcon()
    {
        return $this->icon;
    }
    
    public function hasPage($page)
    {
        return isset($this->pages_map[$page]);
    }
    
    public function getPage($page)
    {
        return $this->pages_map[$page];
    }
    
    public function getPath()
    {
        return $this->path;
    }
    
    public function addDependence($item)
    {
        $this->requires[] = $item;
    }
    
    public function getDependences()
    {
        return $this->requires;
    }
    public function addCliCommand($command)
    {
        $this->cliCommands[] = $command;
    }
    
    public function getCliCommand()
    {
        return $this->cliCommands;
    }
    
    
    public function addSetupStep($item)
    {
        $this->setupSteps[] = $item;
    }
    
    public function getSetupSteps()
    {
        return $this->setupSteps;
    }
    public function addBuildStep($item)
    {
        $this->buildSteps[] = $item;
    }
    
    public function getBuildSteps()
    {
        return $this->buildSteps;
    }
    public function autoloader()
    {
        $folders[] = $this->path . "/src";
        $folders[] = $this->path . "/vendor";
        $files[] = "autoloader.php";
        $files[] = "autoload.php";

        foreach ($folders as $folder) {
            foreach($files as $file) {
                $filename = $folder . "/$file";
                if (file_exists($filename)) {
                    include_once $filename;
                }
            }
        }
        spl_autoload_register(function ($class) use($folders) {
            $file = Functions::getFileByClass($class);
            foreach ($folders as $folder) {
                $filename = $folder . "/$file";
                if(file_exists($filename))
                {
                    include_once $filename;
                    return true;
                }
            }
        });
        if(is_dir($this->path . "/model"))
        {
            // Register model autoloader
            spl_autoload_register(function ($class) {

                $name = $class;
                $namespace = get_class($this);
                $name = str_replace($namespace . "\\" , "", $name);

                $folder = $this->path . "/model";
                $filename = $folder . "/$name.php";
                if(file_exists($filename))
                {
                    include_once $filename;
                    return true;
                }

            });
        }
    }
    public function getModel($name)
    {
        $filename = $this->path . "/model/$name.php";
        if(file_exists($filename))
        {
            require_once $filename;
            $classnames = array(get_class($this) . "\\" . $name, $name  );
            foreach($classnames as $classname)
            {
                if(class_exists($classname))
                {
                    return new $classname();
                }
            }
        }
        return false;
    }

    /**
     * @return WebApp
     */
    public function getApp()
    {
        return $this->app;
    }

    public function onRegisterComponents($app)
    {
    }

    public function onRegisterManagers($app)
    {
    }

    /**
     * @param $task
     * @param array $args
     * @param string $minute
     * @param string $hour
     * @param string $day
     * @param string $month
     * @param string $week
     */
    public function addCronJob($task, $args = array(), $minute = "*", $hour = "*", $day = "*", $month = "*", $week = "*")
    {
        $job = new CronTask();
        $job->setTask( $task );
        $job->setArgs( $args );
        $job->setTask( $task );
        $job->setCron( "$minute $hour $day $month $week" );
        $job->setPlugin( $this );
        $this->cronJobs[] = $job;
    }

    /**
     * @return CronTask[]
     */
    public function getCronJobs()
    {
        return $this->cronJobs;
    }


    public function addWebSocket($web_socket)
    {
        $this->webSockets[] = $web_socket;
    }

    public function getWebSockets()
    {
        return $this->webSockets;
    }

    public function getConfiguration()
    {
        return array();
    }




}