<?php

namespace Dunp\Plugin;

class Controller extends \Dunp\Controller
{
    public function __construct($app, $pagename, $basedir) {
        parent::__construct($app, $pagename, $basedir);
    }
    
    // Retuns some like: plugins/<pname>/controller/<pname>.js
    public function getControllerFile() {
        return ( basename( $this->getBaseDir()) . "/resources/controller/" . $this->getPageName() . ".js");
    }

}