<?php

namespace Dunp;

//use Dunp\Plugin\View;

class Controller
{
    /**
     *
     * @var WebApp
     */
    private $app;
    private $pagename;
    private $basedir;
    /**
     *
     * @var Auth\AuthProvider
     */
    private $authProvider;
    private $authLevel;
    private $adminActions = array();
    private $template = null;
    private $template_data = array();
    var $errors;

    /**
     * @param $app WebApp
     * @param $pagename
     * @param $basedir
     */
    public function __construct($app, $pagename, $basedir) {
        $this->app = $app;
        $this->app->registerController($this);
        $this->pagename = $pagename;
        $this->basedir = $basedir;
        $this->authProvider = new Auth\AnonymousAuthProvider();
        $this->authLevel = AUTHORIZATION_LEVEL_ANONYMOUS;
    }

    public function run($view)
    {
        $app = $this->getApp();
        $this->onStart();
        if ($this->isLoginRequired() && !($app->isLogin() || $this->getAuthProvider()->isLogin() || $this->isLogin()) ) {
            $app->forceLogin();
        } else {

            if ($this->getAuthorizationLevel() <= $this->getAuthProvider()->getRequestAuthorizationLevel($_SERVER)) {

                try
                {
                    $this->process($view);
                }
                catch(\Exception $ex)
                {
                    $this->getApp()->error("Internal Error: " . $ex->getMessage() . "\n" . $ex->getTraceAsString());
                }
            } else {
                $this->getApp()->error("Forbidden: " . $this->getAuthProvider()->getAccessDeniedMessage());
            }
        }
        return true;
    }
    ////////////////////////////////
    //// Semi-Abstract functions
    ////////////////////////////////

    public function render($template, $data = array())
    {
        $this->template = $template;
        $this->template_data = $data;
    }

    /**
     * @param $view View
     * @return bool
     */
    public function process($view)
    {
        $res = $view->display();
        if($res === NULL || $res)
        {
            $view->render($this->template, $this->template_data);
        }
        return true;
    }

    public function onCreate()
    {
        return false;
    }
    /**
     * This method is called when the app start
     * @return boolean
     */
    public function onStart()
    {
        return false;
    }
    
    /**
     * This method is called when the app shutdown
     * @return boolean
     */
    public function onStop()
    {
        return false;
    }
    
    /**
     * Called when the app has no default document. Returns true if the class is invoked
     * @param array $params Array of url parameters (param1/param2/param3/..../paramN)
     * @return boolean
     */
    public function onHandleUrl($params)
    {
        return false;
    }
    /**
     * Return if this commponent need a loged user
     * @return boolean
     */
    public function isLoginRequired()
    {
        return false;
    }
    
    /**
     * Register new methods for api
     * @param \Dunp\Slim\Slim $api
     * @return boolean
     */
    public function onRegisterAPI(\Slim\Slim & $api, $callback)
    {
        
        return false;
    }
    
    public function getConfigurationsTemplate()
    {
        return array();
    }
    /**
     * Returns array of strings to the extra javascripts paths
     * @return type
     */
    public function getScripts()
    {
        return array();
    }
    
    public function getTemplateGlobals()
    {
        return array();
    }
    
    public function getTemplateFilters()
    {
        return array();
    }
    /**
     * 
     * @return WebApp
     */
    public function getApp()
    {
        return $this->app;
    }
    
    public function getBaseDir()
    {
        return $this->basedir;
    }
    
    public function getPageName()
    {
        return $this->pagename;
    }
    
    public function getControllerFile()
    {
        return "pages/controller/" . $this->pagename . ".js";
    }
    
    public function getTemplateData()
    {
        return array();
    }
    
    /**
     * 
     * @param type $table
     * @return \Dunp\Model\Model
     */
    public function getModel($name)
    {
        return $this->getApp()->getDatabaseController()->getTable($name);
    }
    
    
    public function setAuthProvider($provider)
    {
        $this->authProvider = $provider;
    }
    
    /**
     * 
     * @return Auth\AuthProvider
     */
    public function getAuthProvider()
    {
        return $this->authProvider;
    }
    
    public function setAuthorizationLevel($level)
    {
        $this->authLevel = $level;
    }
    
    public function getAuthorizationLevel()
    {
        return $this->authLevel;
    }
    
    public function addAdminAction($action)
    {
        $this->adminActions[] = $action;
    }
    
    public function getAdminActions()
    {
        return $this->adminActions;
    }

    public function addError($error, $index = 0)
    {
        if(is_array($error))
        {
            if(isset($error['message']))
            {
                $this->errors[] = $error;
            }
            else
            {
                foreach($error as $err)
                {
                    $this->addError($err, $index + 1);
                }
            }
        }
        else{

            $this->errors[] = Functions::getErrorTrace($error, $index);
        }
    }

    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return null
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param null $template
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }

    public function isLogin()
    {
        return false;
    }

    /**
     * @return \users
     */
    public function getUser()
    {
        return $this->getAuthProvider()->get();
    }


}