<?php
/**
 * Created by PhpStorm.
 * User: manus
 * Date: 11/6/15
 * Time: 19:11
 */

namespace Dunp;


define("INTERNAL_LOG_DEBUG", "debug");
define("INTERNAL_LOG_WARNING", "warning");
define("INTERNAL_LOG_ERROR", "error");
define("INTERNAL_LOG_VERBOSE", "verbose");
class Log {

    private static $colors = array(
        'error' => '31',
        'warning' => '33',
        'debug' => '39',
        'verbose' => '36'
    );

    public static function log($type, $message)
    {
        if(Functions::isConsoleSession())
            Log::echo_colored($message, Log::$colors[$type]);
        else
            file_put_contents(Functions::getFilePath("system.log"), "[$type]\t". $message . "\n", FILE_APPEND);

    }
    public static function d($message)
    {
        self::log(INTERNAL_LOG_DEBUG, $message);
    }
    public static function v($message)
    {
        self::log(INTERNAL_LOG_DEBUG, $message);
    }
    public static function e($message)
    {
        self::log(INTERNAL_LOG_ERROR, $message);

    }
    public static function w($message)
    {
        self::log(INTERNAL_LOG_WARNING, $message);

    }

    private static function echo_colored($text, $color)
    {
        $string = $text;
        if(!empty($color))
        {
            $string = "\033[" . $color . "m" . $text . "\033[0m\n";
        }

        echo $string;
    }
} 