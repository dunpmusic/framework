<?php

namespace Dunp;

class Functions
{   
    public static function getRandomString($length = 10, $uc = TRUE, $n = TRUE, $sc = FALSE) {
        $source = 'abcdefghijklmnopqrstuvwxyz';
        if ($uc == 1)
            $source .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        if ($n == 1)
            $source .= '1234567890';
        if ($sc == 1)
            $source .= '|@#~$%()=^*+[]{}-_';
        if ($length > 0) {
            $rstr = "";
            $source = str_split($source, 1);
            for ($i = 1; $i <= $length; $i++) {
                mt_srand((double) microtime() * 1000000);
                $num = mt_rand(1, count($source));
                $rstr .= $source[$num - 1];
            }
        }
        return $rstr;
    }

    public static function getFilePath($name = "")
    {
        global $config;
        if($config['site.configured'])
        {
            return $config['upload.directory'] . "/$name";
        }
        return ("/tmp/$name");
    }

    public static function getFileByClass($class)
    {
        return get_file_by_class($class);
    }

    public static function isConsoleSession()
    {
        return isset($_SERVER['argc']);
    }

    public static function isProcessRunning($pid, $isFile = false)
    {
        if($isFile ||file_exists($pid))
        {
            if(!file_exists($pid)) return false;
            $pid = file_get_contents($pid);
        }

        try{
            $result = exec(sprintf("ps %d 2>/dev/null", $pid), $output);
            if( count($output) >= 2){
                return true;
            }
        }catch(Exception $e){}
        return false;
    }

    public static function kill($pid, $isFile = false)
    {
        if(!self::isProcessRunning($pid, $isFile)) return false;
        if($isFile)
        {
            if(!file_exists($pid)) return false;
            $pid = file_get_contents($pid);
        }

        exec("kill $pid");
        if($isFile)
        {
            unlink($pid);
        }
    }

    public static function startProcess($command, $async = true, $logFile = "/dev/null")
    {
        if($async)
        {
            return exec(sprintf("%s > %s 2>&1 & echo $!", $command, $logFile));
        }
        else{

            return exec(sprintf("%s > %s 2>&1 && echo $!", $command, $logFile));
        }
    }

    public static function getErrorTrace($message, $offset = 0)
    {
        $trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
        return array("message" => $message, "where" => ($trace[$offset + 1]));
    }

    public static function setArrayData($object, $array)
    {
        foreach($array as $key => $value)
        {
            $object->{$key} = $value;
        }
    }

    public static function mergeObjects($a, $b)
    {
        foreach($b as $key => $value)
        {
            if(!empty($value))
                $a->$key = $value;
        }
    }

    public static function buildComplexArray($array, $keyName = "key", $valueName = "value")
    {
        $result = array();
        foreach($array as $key => $value)
        {
            $result[] = array($keyName => $key, $valueName => $value);
        }
        return $result;
    }
}

