<?php
namespace Dunp;

class Request
{
    public static function removeQueryParams($input) {
        if (strpos($input, "?") !== FALSE) {
            $input = substr($input, 0, strpos($input, "?"));
        }
        return $input;
    }

    public static function getParameters() {
        $url = Request::removeQueryParams(substr($_SERVER['REQUEST_URI'], 1));

        return $url;
    }

    public static function getParameter($index = 0) {
        $params = Request::getParams();
        if($index < 0)
        {
            $index += count($params);
        }
        if(isset($params[$index ]))
            return Request::removeQueryParams($params[$index ]);
    }

    public static function getParams() {
        $array = ( explode("/", Request::removeQueryParams($_SERVER['REQUEST_URI'])) );
        array_shift($array);
        return $array;
    }
}
