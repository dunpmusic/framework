<?php

namespace Dunp;

class Scheme
{
    
    var $method;
    var $domain;
    var $port;
    var $path;
    
    
    public function __construct($method = null, $domain = null, $port = null, $path = array()) {
        if($method == null) $method = ".*";
        if($domain == null) $domain = ".*";
        if($port == null) $port = ".*";
        $this->method = $method;
        $this->domain = $domain;
        $this->port = $port;
        $this->path = $path;
    }
    
    public static function parseUri($uri)
    {
        $scheme = new Scheme();
        preg_match('/(\w+):\/\/([a-zA-Z0-9-_.%]+)(:([0-9]+))?\/?(.+)?/', $uri, $matches);
//        echo "$uri\n";
//        print_r($matches);

        $scheme->method = strval($matches[1]);
        $scheme->domain = strval($matches[2]);

        if(isset($matches[3]))
            $scheme->port = strval($matches[3]);
        if(isset($matches[5]))
            $scheme->path = explode("/", $matches[5]);
        return $scheme;
        
    }
    
    private static function path_map($path)
    {
        return urldecode($path);
    }
    
    public function equals($scheme)
    {
        if(!$scheme instanceof Scheme)
        {
            $scheme = Scheme::parseUri($scheme);
        }
        if(preg_match("/" . $this->method . "/", $scheme->method))
        {
            if(preg_match("/" . $this->domain . "/", $scheme->domain))
            {
                if(preg_match("/" . $this->port . "/", $scheme->port))
                {
                    $bool = true;
                    if(count($this->path) > 0)
                    {

                        foreach ($this->path as $key => $value) {
                            if(isset($scheme->path[$key]))
                            {
                                if(!preg_match("/" . $this->path[$key] . "/", $scheme->path[$key]))
                                {
                                    $bool = false;
                                    break;
                                }
                            }
                            else
                            {
                                $bool = false;
                                break;
                            }
                        }
                    }
                    return $bool;
                }
            }
        }
        
        return false;
    }

    function __toString()
    {
        return $this->method . "://" . $this->domain . ($this->port != ".*"?$this->port:""). "/" . implode("/", $this->path);
    }


}