<?php
/**
 * Created by PhpStorm.
 * User: manus
 * Date: 9/6/15
 * Time: 16:52
 */

namespace Dunp;


abstract class Manager {

    /**
     * @var WebApp
     */
    private $app;
    private $keyName;

    function __construct($app)
    {
        $this->app = $app;
    }

    public abstract function load();

    /**
     * @return mixed
     */
    public function getKeyName()
    {
        return $this->keyName;
    }

    /**
     * @param mixed $keyName
     */
    public function setKeyName($keyName)
    {
        $this->keyName = $keyName;
    }

    /**
     * @return WebApp
     */
    public function getApp()
    {
        return $this->app;
    }




} 