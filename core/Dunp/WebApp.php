<?php


namespace Dunp;

use Dunp\Cron\CronManager;
use Dunp\Database\DatabaseController;
use Dunp\Database\MySQLController;
use Dunp\Plugin\PluginManager;
use Dunp\WebSocket\WebSocketManager;

define("CONFIG_MANAGER", '001CONFIG_MANAGER');
define("DEFINITIONS_MANAGER", '002DEFINITIONS_MANAGER');
define("DATABASE_MANAGER", '003DATABASE_MANAGER');
define("PLUGIN_MANAGER", '004PLUGIN_MANAGER');
define("CRON_MANAGER", '005CRON_MANAGER');
define("WEBSOCKET_MANAGER", '006WEBSOCKET_MANAGER');
class WebApp
{
    
    /**
     *
     * @var \Cookies
     */
    private $cookies;
    /**
     *
     * @var \Language
     */
    private $language;
    private $plugins;
    private $controllers = array();
//    private $configManager;
//    private $definitionsManager;
    private $databaseController;
    private $defaultTheme;
    private $defaultThemePath;
    private $defaultLocale = 'en_US';
    /**
     * @var Manager[]
     */
    private $managersContainer = array();
    private $sessionId;
    private $timer;
    public function __construct($folder = "") {
        
        $folder .= ($folder == "")?"":"/";
        $this->addManager(CONFIG_MANAGER, new ConfigManager($this));
        $this->addManager(DEFINITIONS_MANAGER, new DefinitionsManager($this, __DIR__ . "/../../"));
        $this->addManager(DATABASE_MANAGER, new DatabaseController($this));
        $this->addManager(PLUGIN_MANAGER, new PluginManager($this, $folder . "plugins"));
        $this->addManager(CRON_MANAGER, new CronManager($this));
        $this->addManager(WEBSOCKET_MANAGER, new WebSocketManager($this));

        $this->defaultTheme = "bootstrap";
        $this->defaultThemePath = "resources/themes";
        
        $this->cookies = new View\Cookies();
        $this->language = new View\Language();
        $this->sessionId = substr( md5(microtime()),0,6);
        $this->timer = start_timer();
        Log::d("Session ID: " . $this->sessionId);
//        $this->configManager = new ConfigManager($this);
//        $this->databaseController = new DatabaseController($this);

//        $this->addManager(DEFINITIONS_MANAGER, new DefinitionsManager($this));
//        $this->definitionsManager = new DefinitionsManager();
//        $this->definitionsManager->load(__DIR__ . "/../../");
//        $this->plugins = new PluginManager($this, $folder . "plugins");



        global $LANGUAGE;
        
        $LANGUAGE = $this->language;
        
    }

    /**
     * @param $name
     * @param $manager Manager
     */
    public function addManager($name, $manager)
    {
        $this->managersContainer[$name] = $manager;
    }

    /**
     * @param $name
     * @return Manager
     */

    public function getManager($name)
    {
        return $this->managersContainer[$name];
    }

    public function initManagers()
    {
        ksort($this->managersContainer);
        reset($this->managersContainer);
        $count = 0;
        while($manager = current($this->managersContainer))
        {
            $timer = start_timer();
            $key = key($this->managersContainer);
            $manager->load();
            $manager->setKeyName($key);
            next($this->managersContainer);
            Log::e( "$key => " . stop_timer($timer) );
            $count++;
        }
    }
    
    /**
     * 
     * @return DefinitionsManager
     */
    public function getDefinitionsManager()
    {
        return $this->getManager(DEFINITIONS_MANAGER);
    }
    
    /**
     * 
     * @return \Dunp\Plugin\PluginManager
     */
    public function getPluginManager()
    {
        return $this->getManager(PLUGIN_MANAGER);
    }
    
    /**
     * 
     * @return ConfigManager
     */
    public function getConfigManager()
    {
        return $this->getManager(CONFIG_MANAGER);
    }
    public function registerController($controller)
    {
        $this->controllers[] = $controller;
    }
    
    /**
     * 
     * @return Database\DatabaseController
     */
    public function getDatabaseController()
    {
        return $this->getManager(DATABASE_MANAGER);
    }
    /**
     * 
     * @return View\Cookies
     */
    function getCookies()
    {
        return $this->cookies;
    }
    /**
     * 
     * @return View\Language
     */
    function getTranslation()
    {
        return $this->language;
    }
    
    public function getScripts()
    {
        return \Helper::recursive("js");
    }
    
    public static function getGetRequest() {
        $array = array();
        if(isset($_SERVER['REQUEST_URI']))
        {
            $pos = strpos($_SERVER['REQUEST_URI'], "?");
            if ($pos > 0) {
                $sub = substr($_SERVER['REQUEST_URI'], $pos + 1);
                $params = explode("&", $sub);
                foreach ($params as $param) {
                    $opt = explode("=", $param);
                    $array[$opt[0]] = urldecode($opt[1]);
                }
            }
        }
        return $array;
    }
    
    public function getStyles()
    {
        return \Helper::recursive("css", "^\\.map\\.css");
    }
    
    function redirect($url)
    {

        Log::d("Redirecting to $url\n");
        header("Location: " . ($url));
    }
    
    function isLogin()
    {
        return $this->getCookies()->is_set("email") &&  $this->getCookies()->is_set("token");
    }
    
    function forceLogin($login = "/login/%s")
    {
        $url = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        $this->redirect(sprintf($login, base64_encode($url)));
        $this->stop();
    }
    
    function stop($forced = true)
    {
        foreach ($this->controllers as $value) {
            $value->onStop();
        }
        $duration = stop_timer($this->timer);
        Log::d("Time: " . $duration );
        Log::d("");
        if($forced)
            die();
    }
    
    function error($message)
    {
        $message =  "Can not load your request<br>$message";
        $template = new View\Template("bootstrap");
        echo $template->build(new View\ErrorView($this, "", $message));
    }
    
    public function setDefaultTheme($theme)
    {
        $this->defaultTheme = $theme;
    }
    public function setDefaultThemePath($themePath)
    {
        $this->defaultThemePath = $themePath;
    }
    
    public function getDefaultTheme()
    {
        return $this->defaultTheme;
    }
    
    public function getDefaultThemePath($name)
    {
        $plugins = $this->getPluginManager()->getPlugins();
        foreach ($plugins as $plugin) {
            foreach ($plugin->getThemes() as $theme) {
                if($name == $theme)
                {
                    return ( $plugin->getPath() . "/" . $this->defaultThemePath );
                }
            }
        }
        return $this->defaultThemePath;
    }
    
    
    public function getDefaultLocale()
    {
        return $this->defaultLocale;
    }
    
    /**
     * 
     * @param type $locale The default locale for example: 'en_US'
     */
    public function setDefaultLocale($locale)
    {
        $this->defaultLocale = $locale;
    }

    /**
     * @return string
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    function __destruct()
    {
        // TODO: Implement __destruct() method.

        $this->stop(false);
    }


}