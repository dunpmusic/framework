<?php

namespace Dunp\View;

use Dunp\Functions;

class Template {

    var $theme = "";
    var $themes = "";

    private static $globalData = array();

    public function __construct($theme = "default", $themesPath = "resources/themes") {
        $this->theme = $theme;
        $this->themes = $themesPath;
    }

    public static function addGlobalData($data)
    {
        Template::$globalData = array_merge(Template::$globalData, $data);
    }

    public function getThemePath() {
        return $this->themes;
    }

    public function getPath() {
        return $this->getThemePath() . "/" . $this->theme;
    }

    public function getTemplatePath($name) {
        return $this->getPath() . "/" . $name;
    }

    public function getComponentsPath() {
        return "/" . str_replace("../plugins/", "", $this->getPath()) . "/components";
    }

    public function getTemplateFile($template) {
        return $this->getTemplatePath($template) . ".html";
    }

    public function getScriptName($template) {
        return $this->getComponentsPath() . "/js/$template.js";
    }

    public function getStyleName($template) {
        return $this->getComponentsPath() . "/css/$template.css";
    }

    public function getStyleThemeName() {
        return $this->getComponentsPath() . "/css/{$this->theme}.css";
    }

    public function getSectionPath($name) {
        return $this->getPath() . "/section/$name";
    }

    public function getIncludePath($name) {
        return $this->getPath() . "/$name";
    }

    public function getDefaultViewFile() {
        return "resources/empty.html";
    }
    
    /**
     * 
     * @param \Dunp\View $view
     * @return type
     */
    public function getDefaultData($view)
    {
        $default = array();
        $default['tr'] = $view->getController()->getApp()->getTranslation()->getStrings();
        foreach ($view->getController()->getApp()->getTranslation()->getLanguages() as $value) {
            $default['tr']['LANGUAGES'][] = array('id' => $value,
                'name' => $view->getController()->getApp()->getTranslation()->getString("LANGUAGE", $value)
            );
        }
        $default['trf'] = TemplateProcessor::get_multiarray_processor($default['tr'], function($key, $lambda) use($default)
        {
            return sprintf($default['tr'][$key], $lambda);
        });

        $default['timer'] = time();
        $default['parameters'] = \Dunp\Request::getParams();
        $default['request'] = \Dunp\WebApp::getGetRequest();
        $default['locale'] = $view->getController()->getApp()->getTranslation()->getLanguage();
        $default = array_merge($default, Template::$globalData);

        $default['lang'] = $view->getController()->getApp()->getTranslation()->build_js_data();
        $default['app'] = \Helper::buildJSender(array_merge($view->getController()->getTemplateData(), $default));

        $default['path'] = $this->getComponentsPath();
        $default = array_merge((array) $view->getController(), $default);
        $default = array_merge((array) $view->getTemplateData(), $default);
        $default['controller'] = $view->getController();

        return $default;
    }

    /**
     * 
     * @param \Dunp\View $view
     */
    public function build($view) {
        $scripts = $view->getController()->getScripts();
        if (!is_array($scripts)) {
            $scripts = array();
        }
        $styles = $view->getStyles();
        if (!is_array($styles)) {
            $styles = array();
        }
        $headers = array();
        $title = "";
        $template = "";
        $default = $this->getDefaultData($view);
        $getdata = new TemplateData($default);
        $theme = "";
        $componentsPath = $this->getComponentsPath();
        $localStyles = array();
        $getdata->func("style", TemplateProcessor::get_custom_processor(function($text) use(&$localStyles, $view) {
                            $localStyles[] = $text;
                        }))
                ->func("title", TemplateProcessor::get_custom_processor(function($text) use(&$title) {
                            $title = $text;
                        }))
                ->func("template", TemplateProcessor::get_custom_processor(function($text) use(&$template) {
                            $template = $text;
                        }))
                ->func("script", TemplateProcessor::get_custom_processor(function($text) use(&$scripts, $view) {
                            $scripts[] = $text;
                        })
                )
                ->func("theme", TemplateProcessor::get_custom_processor(function($text) use(&$theme) {
                            $theme = $text;
                        })
                )
                ->func("section", TemplateProcessor::get_custom_processor(function($text) use($view) {
                        if (file_exists($view->getSectionPath($text))) {
                            return file_get_contents($view->getSectionPath($text));
                        } else {
                            die("Error. Not found section $text " . $view->getSectionPath($text));
                        }
                        return "";
                    })
                )
            ->func("section", TemplateProcessor::get_custom_processor(function($text) use($view) {
                    if (file_exists($view->getSectionPath($text))) {
                        return file_get_contents($view->getSectionPath($text));
                    } else {
                        die("Error. Not found section $text " . $view->getSectionPath($text));
                    }
                    return "";
                })
            )
            ->func("include", TemplateProcessor::get_custom_processor(function($text) use($view) {
                    if (file_exists($view->getIncludePath($text))) {
                        return file_get_contents($view->getIncludePath($text));
                    } else {
                        return("Error. Not found include $text " . $view->getIncludePath($text));
                    }
                    return "";
                })
            )
//                ->func("debug", function() use($getdata) {
//                    return print_r($getdata, true);
//                })
                ->func('header', TemplateProcessor::get_custom_processor(function($text) use(&$headers) {
                    $headers[] = $text;
                }))
                ->value("request", \Dunp\WebApp::getGetRequest())
                ->value("parameters", \Dunp\Request::getParams())
                ->value("relative", basename($view->getBaseDir()) . "/resources")
                ->value("path", $componentsPath)
                ->value("view", $view)
                ->helper($view->getFilters())
        ;

        if (($custom = $view->getCustomContent()) != null) {
            $page = $getdata->parse($custom);
        } else {
            if($view->getTemplate() !== NULL)
            {
                if(file_exists($view->getViewFile()))
                {
                    $page = $getdata->render($view->getViewFile());
                }
                else{
                    $page = $getdata->render($this->getDefaultViewFile());
                }
            }
            else{
                $page = "";
            }
        }

        if ($theme != "") {
            $this->theme = $theme;
            $this->themes = $view->getController()->getApp()->getDefaultThemePath($this->theme);
        }

        foreach($headers as $header)
        {
            header($header);
        }
        if($template != "")
        {

            if (!file_exists($this->getTemplateFile($template))) {
                $template = "normal";
            }

            $styles[] = $this->getStyleName($template);
            $styles[] = $this->getStyleThemeName();

            $coreScripts = array();
            $staticScripts = array();
            $data = new TemplateData($default);
            $data
                    ->func("section", TemplateProcessor::get_custom_processor(function($text) {
                                if (file_exists($this->getSectionPath($text))) {
                                    return file_get_contents($this->getSectionPath($text));
                                }
                                return "";
                            })
                    )

                    ->func("include", TemplateProcessor::get_custom_processor(function($text) use($view) {
                            if (file_exists($view->getIncludePath($text))) {
                                return file_get_contents($view->getIncludePath($text));
                            } else {
                                return("Error. Not found include $text " . $view->getIncludePath($text));
                            }
                            return "";
                        })
                    )
                    ->func("active", TemplateProcessor::get_active_class_processor())

                    ->func("style", TemplateProcessor::get_custom_processor(function($text) use(&$styles) {
                                $styles[] = $text;
                            })
                    )
                    ->func("script", TemplateProcessor::get_custom_processor(function($text) use(&$coreScripts) {
                                $coreScripts[] = $text;
                            })
                    )
                    ->func("static", TemplateProcessor::get_custom_processor(function($text) use(&$staticScripts) {
                                $staticScripts[] = $text;
                            })
                    )
                    ->func("scripts", TemplateProcessor::get_single_processor(function($text) use(&$scripts, &$coreScripts) {

                                $buff = $this->display_scripts($text, $this->build_scripts($coreScripts, $scripts));

                                $scripts = array();
                                $coreScripts = array();

                                return $buff;
                            })
                    )
                    ->func("statics", TemplateProcessor::get_single_processor(function($text) use(&$staticScripts) {

                                $buff = $this->display_scripts($text, $this->build_scripts($staticScripts));

                                $staticScripts = array();

                                return $buff;
                            })
                    )
                    ->func("styles", TemplateProcessor::get_single_processor(function($text) use(&$styles, &$localStyles) {

                                $styles = array_merge($styles, $localStyles);

                                $buff = $this->display_css($text, $this->build_css($styles) );
    //                            die(print_r($buff));
                                $styles = array();

                                return $buff;
                            })
                    )
                    ->func("lang_js", TemplateProcessor::get_helper_processor(function() use ($view) {
                                return $view->getController()->getApp()->getTranslation()->build_js_data();
                            })
                    )
                    ->func("app", TemplateProcessor::get_helper_processor(function() use ($view, $getdata) {
                                $data = \Helper::buildJSender(array_merge($view->getController()->getTemplateData(), $getdata->data));
                                return "<script> eval(atob('" . base64_encode($data) . "')); </script>";
                            })
                    )
                    ->func("eval", TemplateProcessor::get_custom_processor(function($text) use ($view) {
                                return eval($text);
                            })
                    )
                    ->value("request", \Dunp\WebApp::getGetRequest())
                    ->value("content", $page)
    //                ->value("styles", $styles)
                    ->value("relative", basename($view->getBaseDir()))
                    ->value("basedir", $view->getBaseDir())
                    ->value("path", $componentsPath)
                    ->value("title", $title)
                    ->helper($view->getFilters());

            $templateFile = $this->getTemplateFile($template);
            $scripts[] = $this->getScriptName($template);
            $scripts[] = "/" . $view->getController()->getControllerFile();


            if (!file_exists($templateFile)) {
                echo "Warning base template file '$templateFile' not exists";
            }
            return $data->render($templateFile);
        }
        return $page;
    }

    public function display_css($text, $list) {
        $buff = "";
        foreach ($list as $value) {
            if (strpos($value, "<") !== FALSE) {
                $buff .= $value;
            } else {
                $buff .= str_replace("{{.}}", $value, $text);
            }
        }
        return $buff;
    }
    public function build_css() {
        global $config;

        $buffer = array();
        $cssFiles = array();
        $cssData = array();
        foreach (func_get_args() as $value) {
            $res = array();
            foreach ($value as $sc) {
                if (strpos($sc, "<") !== FALSE) {
                    $cssData[] = $sc;
                } else {
                    
                    if ($config['scripts.compression']) {
                        
                        if(strpos($sc, "http") === 0 && $config['scripts.remote'])
                        {
                            $file = "__http_cache__" . md5($sc) . ".css";
                            $filename = $config['upload.directory'] . "/" . $file;
                            if(!file_exists($filename))
                            {
                                $data = \Dunp\Http\HttpRequest::get($sc);
                                if(file_put_contents($filename, $data))
                                {
                                    $sc = $filename;
                                }
                            }
                            else
                            {
                                $sc = $filename;
                            }
                        }
                        else
                        {
                            if (!file_exists($sc)) {
                                $filename = "../plugins/" . $sc;
                                if (file_exists($filename)) {
                                    $sc = $filename;
                                } else {
                                    $filename = $_SERVER['DOCUMENT_ROOT'] . (strpos($sc, "/")===0?"":"/") . $sc;
                                    if (file_exists($filename)) {
                                        $sc = $filename;
                                    } else {
//                                        die("OMG $filename");
                                    }
                                }
                            }
                        }
                    }

                    $res[] = $sc;
                }
            }

            $cssFiles[] = $res;
        }
        
//        $cssFiles = array_reverse($cssFiles);
//        print_r($scriptFiles);
        foreach ($cssFiles as $value) {
            if ($config['scripts.compression']) {
                $cmd = "ls -lsa " . implode(" ", $value);
                $hash = md5(shell_exec($cmd) . ($config['scripts.minification'] ? "1" : "0"));
                $cache = $config['upload.directory'] . "/_css_" . $hash . ".css";
                if (!file_exists($cache)) {
                    $subuff = "";
                    foreach ($value as $css) {
                        if ($config['scripts.minification']) {
                            $min = new \CSSmin(); //();
                            $subuff .= "/* $css */\n" . $min->run(file_get_contents($css)) . "\n\n";
                        } else {
                            $subuff .= "/* $css */\n" . (file_get_contents($css)) . "\n\n";
                        }
                        
                    }
                    file_put_contents($cache, $subuff);
                }
                $buffer[] = "/machine/$hash/app.css";
            } else {

                foreach ($value as $css) {
                    $buffer[] = $css;
                }
            }
        }
        return array_merge($buffer, $cssData);
    }
    
    public function display_scripts($text, $list) {
        $buff = "";
        foreach ($list as $value) {
            if (strpos($value, "<") !== FALSE) {
                $buff .= $value;
            } else {
                $buff .= str_replace("{{.}}", $value, $text);
            }
        }
        return $buff;
    }

    public function build_scripts() {
        global $config;

        $buffer = array();
        $scriptFiles = array();
        $scriptData = array();
        foreach (func_get_args() as $value) {
            $res = array();
            foreach ($value as $sc) {

                if (strpos($sc, "<") !== FALSE) {
                    $scriptData[] = $sc;
                } else {
                    
                    if ($config['scripts.compression']) {

                            if (!file_exists($sc)) {
                                $filename = "../plugins/" . $sc;
                                if (file_exists($filename)) {
                                    $sc = $filename;
                                } else {
                                    $filename = $_SERVER['DOCUMENT_ROOT'] . "/" . $sc;
                                    if (file_exists($filename)) {
                                        $sc = $filename;
                                    } else {
        //                                die("OMG $sc");
                                    }
                                }
                            }
                    }

                    $res[] = $sc;
                }
            }

            $scriptFiles[] = $res;
        }
        
//        print_r($scriptFiles);
        foreach ($scriptFiles as $value) {
            if ($config['scripts.compression']) {
                $cmd = "ls -lsa " . implode(" ", $value);
                $hash = md5(shell_exec($cmd) . ($config['scripts.minification'] ? "1" : "0"));
                $cache = $config['upload.directory'] . "/_scripts_" . $hash . ".js";
                if (!file_exists($cache)) {
                    $subuff = "";
                    foreach ($value as $script) {
                        if ($config['scripts.minification']) {
                            $min = new \JSMin(file_get_contents($script));
                            $subuff .= $min->min() . ";\n\n";
                        } else {
                            $subuff .= (file_get_contents($script)) . ";\n\n";
                        }
                        
                    }
                    file_put_contents($cache, $subuff);
                }
                $buffer[] = "/machine/$hash/app.js";
            } else {

                foreach ($value as $script) {
                    $buffer[] = $script;
                }
            }
        }
        return array_merge($buffer, $scriptData);
    }

}
