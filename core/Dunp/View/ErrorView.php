<?php

namespace Dunp\View;

class ErrorView extends \Dunp\View
{
    var $message;
    public function __construct($app, $basedir, $message) {
        parent::__construct($app, $basedir);
        $this->message = $message;
    }
    public function display() {
        return true;
    }
    
    public function getCustomContent() {
        return "{{#theme}}bootstrap{{/theme}}{{#template}}bootstrap{{/template}}<div class='alert alert-danger'><pre>" . $this->message . "</pre></div>";
    }
}