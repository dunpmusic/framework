<?php
namespace Dunp\View;

class TemplateContentHelper
{
     const SHORT_DATE = 'd/m/Y';

    public function __isset($key) {
        return method_exists($this, '_'.$key);
    }

    public function __get($key) {
        return array($this, '_'.$key);
    }

    public function iso8601($date) {
        return $this->parse($date)->format(DATE_ISO8601);
    }

    public function atom($date) {
        return $this->parse($date)->format(DATE_ATOM);
    }

    public function short($date, $labda) {
        return $this->parse($date)->format(self::SHORT_DATE);
    }
    
    public function year()
    {
        return date("Y");
    }

    private function parse($date) {
        return new \DateTime($date);
    }
    
}