<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Dunp\View;

/**
 * Description of Cookies
 *
 * @author manus
 */
class Cookies {
    //put your code here
    
    
    public function is_set($name)
    {
        return $this->get($name) != FALSE;
    }
    public function get($name)
    {
        $name = md5($name);
        if(isset($_COOKIE[$name]))
            return $_COOKIE[$name];
        return false;
    }
    
    public function set($name, $value, $expiration = null)
    {
        if($expiration === null)
        {
            $expiration = time()+60*60*24*360;
        }
        setcookie( md5($name), $value, $expiration);
        $_COOKIE[ md5($name) ] = $value;

    }
    
    public function remove($name)
    {
        $this->set($name, '', 0);
    }
    
}
