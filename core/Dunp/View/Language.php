<?php
namespace Dunp\View;

use Dunp\Plugin\Plugin;

define('LANGUAGE_FLAGS', 'img/lang/%s.png');
class Language {

    var $strings = array();
    var $selectedLanguage = "en";

    public function loadFromJSON($language, $json)
    {
        foreach ($json as $key => $value) {
            $this->addString($language, $key, $value);
        }
    }

    /**
     * @param $plugin Plugin
     * @param $language
     */
    public function addTranslationFromPlugin($plugin, $language)
    {
        $filename = $plugin->getPath() . "/lang/$language.json";
        if(file_exists($filename))
        {
            $json = json_decode(file_get_contents($filename), true);
            foreach ($json as $key => $value) {
                $this->addString($language, $key, $value);
            }
        }
    }
    public function isValidLanguage($language_id) {
        return isset($this->strings[$language_id]);
    }

    public function setLanguage($language_id) {
        if($this->isValidLanguage($language_id))
            $this->selectedLanguage = $language_id;
    }

    public function getLanguage() {
        return $this->selectedLanguage;
    }

    public function getLanguageFlag($language = null) {
        if ($language == null)
            $language = $this->selectedLanguage;
        
        return sprintf(LANGUAGE_FLAGS, $language);
        
    }
    public function getStrings($language = null) {
        if ($language == null) {
            $language = $this->selectedLanguage;
        }
        return $this->strings[$language];
    }

    public function addString($language, $string_id, $string_content) {
        if(!isset($this->strings[$language]) )
        {
            $this->strings[$language] = array();
        }
        $this->strings[$language][$string_id] = $string_content;
    }

    public function getString($string_id, $language = null) {
        if ($language == null) {
            $language = $this->selectedLanguage;
        }

        if (key_exists($language, $this->strings) && key_exists($string_id, $this->strings[$language]) && $this->strings[$language][$string_id] != "") {
            return $this->strings[$language][$string_id];
        }

        return "$string_id";
    }
    
    public function issetString($string_id, $language = null) {
        if ($language == null) {
            $language = $this->selectedLanguage;
        }

        if (key_exists($language, $this->strings) && key_exists($string_id, $this->strings[$language]) && $this->strings[$language][$string_id] != "") {
            return true;
        }

        return false;
    }
    
    public function getFormatString($string,$args)
    {
        $argv = func_get_args();
        $string = array_shift( $argv );
        return sprintf( $this->getString($string), $argv );
    }
    public function getLanguages() {
        $langs = array();
        foreach ($this->strings as $key => $value) {
            if (!in_array($key, $langs))
                $langs[] = $key;
        }
        return $langs;
    }

    public function getUserLanguage() {
//        return substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
        if(isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
        {

            if (($locale = \locale_accept_from_http($_SERVER['HTTP_ACCEPT_LANGUAGE'])) != null) {
                return $locale;
            }
            return substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2); //$this->getApp()->getDefaultLocale();
        }
    }
    
    
    public function build_js_data()
    {
        
        $strings = $this->getStrings();
        $lang = $this->getLanguage();
        $str = "";
        foreach ($strings as $key => $value) {
            $str .= "Translate.add('$lang', '" . base64_encode($key) . "','" . base64_encode(utf8_decode($value)) . "');\n";
        }
        $str .= "\nTranslate.setLanguage('$lang');\n"; 
        return $str;
    }

}
