<?php

namespace Dunp\View\Twig;

abstract class TwigView extends \Dunp\Plugin\View
{
    
    
    
    protected function build($theme, $themePath) {
        $twig = new TwigTemplate($theme, $themePath);
        
        return $twig->build($this);
        
        
    }
    
    public function getViewFile() {
        return parent::getViewFile() . ".twig";
    }
    
}