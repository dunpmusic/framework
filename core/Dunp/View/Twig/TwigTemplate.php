<?php

namespace Dunp\View\Twig;

use Dunp\Functions;

class TwigTemplate extends \Dunp\View\Template
{
    
    private static $extensions = array();
    
    public static function addCustomExtension($extension)
    {
        if(!in_array($extension, TwigTemplate::$extensions))
        {
            TwigTemplate::$extensions[] = $extension;
        }
    }
    
    /**
     * 
     * @param TwigView $view
     */
    public function build($view) {
        $themePath = $this->themes . "/" . $this->theme;
        $path = array(
            dirname($view->getViewFile()),
            $themePath
        );
        $data = $this->getDefaultData($view);
        
        $loader = new \Twig_Loader_Filesystem($path);
        
        $twig = new \Twig_Environment($loader, array(
            'cache' => Functions::getFilePath("templates"),
            'auto_reload' => true
        ));
        foreach (TwigTemplate::$extensions as $value) {
            $twig->addExtension($value);
        }
        
        $twig->addExtension(new TwigCustomExtension($this, $view));
        $data['view'] = $view;
        return $twig->render(basename($view->getViewFile()), $data);
    }
    
    public function addFunction($twig, $name, $func) {
        $twig->addFunction($name, new \Twig_SimpleFunction($name, $func));
    }
}