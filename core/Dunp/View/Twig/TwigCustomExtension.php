<?php


namespace Dunp\View\Twig;
class TwigCustomExtension extends \Twig_Extension
{
    var $functions = array();
    public function __construct(TwigTemplate $template, \Dunp\View $view) {
        $this->addFunction('asset', function($text, $type = "view") use($template, $view)
        {
            $file = $text;
            if($type == "theme")
            {
                $file = $template->getComponentsPath() . "/" . $text;
            }
            else
            {
                $file = "/" . basename($view->getBaseDir()) . "/resources/" . $text;
            }
            return $file;
        });
    }
    
    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('base64encode', function($data)
            {
                return base64_encode($data);
            })
        );
    }
    public function getName() {
        "framework";
    }
    public function getFunctions() {
        return $this->functions;
    }
    public function addFunction($name, $func) {
        $this->functions[] = new \Twig_SimpleFunction($name, $func);
    }
    

    
}