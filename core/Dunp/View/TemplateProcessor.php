<?php
namespace Dunp\View;


class TemplateProcessor {

    public static function get_text_processor($prefix = "", $subfix = "") {
        return function($text, \Mustache_LambdaHelper $helper) use($prefix, $subfix ) {
            return $prefix . ($helper->render($text) . $subfix);
        };
    }

    public static function get_custom_processor($parse_callback) {
        return function($text, \Mustache_LambdaHelper $helper) use($parse_callback) {
            return $parse_callback($helper->render($text));
        };
    }
    public static function get_single_processor($parse_callback) {
        return function($text) use($parse_callback) {
            return $parse_callback(isset($text)?$text:"");
        };
    }

    public static function get_inverse_processor($parse_callback) {
        return function($text, \Mustache_LambdaHelper $helper) use($parse_callback) {
            return ($helper->render($parse_callback($text)));
        };
    }
    public static function get_array_processor($parse_callback) {
        return function($text, \Mustache_LambdaHelper $helper) use($parse_callback) {
            $data = $parse_callback();
            
            return ($helper->render($text));
        };
    }

    public static function get_active_class_processor() {
        return function($text) {
            if (strpos($_SERVER['REQUEST_URI'], $text) !== FALSE) {
                $class = "active ";
            } else {
                $class = "";
            }
            return " href=\"$text\" class=\"$class\"";
        };
    }


    public static function get_helper_processor($parse_callback) {
        return function() use($parse_callback) {
            return $parse_callback();
        };
    }
    public static function get_multiarray_processor($array, $parse_callback) {
        $result = array();
        foreach($array as $key => $value)
        {
            $result[$key] = self::get_custom_processor(function($result) use($key, $parse_callback)
            {
                return $parse_callback($key, $result);
            });
        }
        return $result;
    }
    
//    public static function 

}
