<?php

namespace Dunp\View;

use Dunp\Functions;

class TemplateData {

    public function __construct($default = array()) {
        $this->data = $default;
        $this->helpers['date'] = array(
            'year' => function($time)
            {
                return date("Y", $time);
            }
        );
        $this->helpers['debug'] = function($data)
        {
            return print_r($data, true);
        };
        $this->helpers['basename'] = function($data)
        {
            return basename($data);
        };
    }

    var $data = array();
    var $helpers = array();

    /**
     * 
     * @param type $name
     * @param type $value
     * @return TemplateData
     */
    public function value($name, $value) {
        $this->data[$name] = $value;
        return $this;
    }

    /**
     * 
     * @param type $name
     * @param type $filename
     * @return \TemplateData
     */
    public function file($name, $filename) {
        if (file_exists($filename)) {
            $this->data[$name] = $this->render($filename);
        }
        return $this;
    }

    public function parse($data)
    {
        global $config;
        $m = new \Mustache_Engine(array(
            'cache' => Functions::getFilePath(),
            'cache_file_mode' => 0666, // Please, configure your umask instead of doing this :)
            'pragmas' => [\Mustache_Engine::PRAGMA_FILTERS],
        ));
        foreach ($this->helpers as $key => $value) {
            $m->addHelper($key, $value);
        }
        return $m->render($data, $this->data); // "Hello, World!"
    }
    /**
     * 
     * @param type $name
     * @param type $filename
     * @param TemplateData $data
     * @return TemplateData
     */
    public function render($filename) {
        if (file_exists($filename)) {
            try
            {

                return $this->parse(file_get_contents($filename));
            }
            catch(\Exception $ex)
            {
                throw new \Exception($ex->getMessage() . " " . $filename, $ex->getCode(), $ex);
            }
        }
        return false;
        
    }

    /**
     * 
     * @param type $name
     * @param type $callable
     * @return TemplateData
     */
    public function func($name, $function) {
        $this->data[$name] = $function;
        return $this;
    }

    /**
     * @param $name
     * @param null $function
     * @return $this
     */
    public function helper($name, $function = null) {
        if(is_array($name))
        {
            foreach($name as $key => $value)
            {
                $this->helpers[$key] = $value;
            }
        }
        else{
            $this->helpers[$name] = $function;
        }
        return $this;
    }


    public function arr($array) {
        $this->data = array_merge($array, $array);
        return $this;
    }

}
