<?php

namespace Dunp\Auth;

define("AUTHORIZATION_LEVEL_ANONYMOUS",  -0x001);
define("AUTHORIZATION_LEVEL_USER",       0x000);
define("AUTHORIZATION_LEVEL_MODERATOR",  0x0f0);
define("AUTHORIZATION_LEVEL_ADMIN",      0x0ff);
define("AUTHORIZATION_LEVEL_HOST",       0xf00);

abstract class AuthProvider
{
    protected $loginUrl;
    protected $autoredirect;
    public abstract function getRequestAuthorizationLevel($request);
    
    public function getAccessDeniedMessage()
    {
        return "Restricted area please contact your administrator";
    }

    /**
     *
     * Run the authorization provider
     * @param array $data The proper data to run the provider
     */
    public function runAuthorization($data = array())
    {

        return false;
    }

    public function logout()
    {

    }

    /**
     * @return users
     */
    public function get()
    {

    }

    public abstract function isLogin();


    public function setLoginUrl($url, $autoredirect = true)
    {
        $this->loginUrl = $url;
        $this->autoredirect = $autoredirect;
    }
}