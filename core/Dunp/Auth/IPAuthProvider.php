<?php

namespace Dunp\Auth;
use Dunp\Functions;

class IPAuthProvider extends AuthProvider
{
    var $adminIpsFile;
    
    public function __construct() {
        global $config;
        
        $this->adminIpsFile = realpath(Functions::getFilePath()) . "/ip.txt";
    }
    public function getRequestAuthorizationLevel($request) {
        if(file_exists($this->adminIpsFile))
        {
            $ips = explode("\n", file_get_contents($this->adminIpsFile));
            if(in_array($request['REMOTE_ADDR'], $ips))
            {
                return AUTHORIZATION_LEVEL_ADMIN;
            }
        }
        return AUTHORIZATION_LEVEL_ANONYMOUS;
    }
    
    public function getAccessDeniedMessage() {
        parent::getAccessDeniedMessage();
        return "Your ip <code>" . $_SERVER['REMOTE_ADDR'] . "</code> is not registered as admin ip. Please create/edit: " . $this->adminIpsFile;
    }

    public function isLogin()
    {
        // TODO: Implement isLogin() method.
        return true;
    }
}