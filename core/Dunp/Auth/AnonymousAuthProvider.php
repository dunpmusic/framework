<?php

namespace Dunp\Auth;

class AnonymousAuthProvider extends AuthProvider
{
    public function getRequestAuthorizationLevel($request) {
        return AUTHORIZATION_LEVEL_ANONYMOUS;
    }

    public function isLogin()
    {
        // TODO: Implement isLogin() method.
        return true;
    }
}