<?php

namespace Dunp;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Used by API to transfer data. It takes all classes of super type Transfereable, and evaluate the functions 
 *  that start with '_' (userscore) and add to simple array using key the name of function. 
 *
 * @author Manus
 */
abstract class Transfereable {
    
    private $additional_params = array();
    public function build($max_depth = 512, $depth = 0) {
        if ($depth > $max_depth) {
            error_log("Transfereable::build depth reached!");
            return null;
        }
        $depth++;
        $list = (get_class_methods($this));
        $array = array();
        foreach ($list as $method) {

            if (strpos($method, "__") === 0)
                continue;
            if (strpos($method, "_") === 0) {
                $mn = substr($method, 1);
                $data = $this->$method();
                if (strpos($mn, "get_") === 0) {
                    $mn = substr($mn, 4);
                }
                if ($data instanceof Transfereable) {
                    $array[$mn] = $data->build($max_depth, $depth);
                } else {
                    if (is_array($data)) {
                        $list = array();
                        foreach ($data as $key => $value) {
                            if ($value instanceof Transfereable) {
                                $list[$key] = $value->build($max_depth, $depth);
                            } else {
                                $list = $data;
                                break;
                            }
                        }
                        $array[$mn] = $list;
                    } else {
                        $array[$mn] = $data;
                    }
                }
            }
        }
        foreach ($this->additional_params as $key => $item) {
            $array[$key] = $item;
        }
        return $array;
    }
    
    public function register_additional_parameter($name, $value)
    {
        $this->additional_params[$name] = $value;
    }

}
