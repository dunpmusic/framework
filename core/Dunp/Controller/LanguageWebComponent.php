<?php

namespace Dunp\Controller;

class LanguageWebComponent extends WebComponent {

    public function __construct($app, $priority = 10) {
        parent::__construct($app, $priority);
        $this->addScheme(new \Dunp\Scheme());
    }

    public function getParameter() {
        if($this->isValidLanguage())
            return \Dunp\Request::getParameter(1);
        return \Dunp\Request::getParameter(0);
    }

    public function getLanguage() {
        return \Dunp\Request::getParameter(0);
    }

    public function getLanguageFile($plugin, $lang) {
        return $plugin->getPath() . "/lang/$lang.json";
    }

    public function isLanguageExists($plugin, $lang) {
        return file_exists($this->getLanguageFile($plugin, $lang));
    }

    public function isValidLanguage() {
        $lang = $this->getLanguage();
        return preg_match("/[a-z]+_[A-Z]+/", $lang) === 1;
    }

    public function validateLanguage($plugin) {
        $lang = $this->getDefaultLocale();
        $url = "/$lang" . $_SERVER['REQUEST_URI'];
        header("Location: " . $url);
    }

    public function setLanguage($plugin) {
        $lang = $this->getLanguage();

        $json = file_get_contents($this->getLanguageFile($plugin, $lang));
        if ($json) {
            if ($json = json_decode($json)) {
                $this->getApp()->getTranslation()->loadFromJSON($lang, $json);
                $this->getApp()->getTranslation()->setLanguage($lang);
                return true;
            }
        }
        return false;
    }

    /**
     * 
     * @param \Dunp\Plugin\Plugin $plugin
     */
    public function getLanguages($plugin) {
        $langs = array();
        $files = scandir($plugin->getPath() . "/lang");
        foreach ($files as $value) {
            if ($value != "." && $value != "..") {
                $index = strrpos($value, ".");
                if (substr($value, $index) == ".json") {
                    $langs[] = substr($value, 0, $index);
                }
            }
        }
        return $langs;
    }

    public function getCookieLang($plugin) {
        $items = $this->getApp()->getGetRequest();
        if (isset($items['lang']) && $this->isLanguageExists($plugin, $items['lang'])) {
            $this->getApp()->getCookies()->set('lang', $items['lang']);
        }
        if ($this->getApp()->getCookies()->is_set("lang")) {
            return $this->getApp()->getCookies()->get("lang");
        }
        return false;
    }

    public function getDefaultLocale() {
        if (($locale = \locale_accept_from_http($_SERVER['HTTP_ACCEPT_LANGUAGE'])) != null) {
            return $locale;
        }
        return $this->getApp()->getDefaultLocale();
    }

//    
//    protected function render($page, $plugin) {
//        parent::render($page, $plugin);
    /**
     * 
     * @param \Dunp\Plugin\View $page
     * @param type $plugin
     */
    protected function render($page, $plugin) {
        if ($this->isValidLanguage()) {
            if ($this->setLanguage($plugin)) {
                $page->getController()->run($page);
            } else {
                header("Location: /");
                $this->diplay_error("Error loading laguage in language");
            }
        } else {
            $this->validateLanguage($plugin);
        }
    }

    public function getLanguagesTable() {
        return array(
            "en_US" => "English",
            "es_ES" => "Español");
    }

}
