<?php

namespace Dunp\Controller;

class CliComponent extends \Dunp\Component {

    /**
     * 
     * @param \Dunp\Plugin\Plugin $plugin
     * @param type $command
     */
    public function runCommand($plugin, $command, $argv) {
        $name = "{$command}";
        $classname = "{$command}_cli";
        $filename = $plugin->getPath() . "/cli/$name.php";
        if (file_exists($filename)) {
            include $filename;

            if (class_exists($classname)) {
                $class = new $classname($this->getApp());
                if ($class instanceof \Dunp\Plugin\Cli) {
                    return $class->run(count($argv), array_map(function($a) {
                                        return urldecode($a);
                                    }, $argv));
                }
            }
        }
    }

    /**
     * 
     * @param \Dunp\Scheme $scheme
     */
    public function run($scheme) {



        if (isset($scheme->path[0])) {

            $name = $scheme->path[0];
            $plugins = $this->getApp()->getPluginManager()->getPlugins();
            foreach ($plugins as $plugin) {
                $commands = $plugin->getCliCommand();
                foreach ($commands as $value) {
                    if ($name == $value) {
                        if ($this->runCommand($plugin, $value, array_splice($scheme->path, 0))) {
                            return true;
                        } else {
                            $this->error("Command execution error");
                            return false;
                        }
                    }
                }
            }
            $this->error("Command not found");
        } else {
            echo "Available commands:\n";
            $plugins = $this->getApp()->getPluginManager()->getPlugins();
            foreach ($plugins as $plugin) {
                echo "Plugin: '{$plugin->getNamespace()}'\n";
                $commands = $plugin->getCliCommand();
                foreach ($commands as $value) {
                    echo "\t" . $value . "\n";
                }
            }
        }
    }

    public function error($text) {

        echo "ERROR: $text\n";
    }

}
