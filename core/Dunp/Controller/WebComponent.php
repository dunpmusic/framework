<?php

namespace Dunp\Controller;

use Dunp\View\CustomView;
use Dunp\View\Template;

class WebComponent extends \Dunp\Component
{

    static $minification = false;
    static $compression = false;

    public function __construct($app, $priority = 10)
    {
        parent::__construct($app, $priority);
    }

    /**
     *
     * @param \Dunp\View $page
     * @param \Dunp\Plugin\Plugin $plugin
     */
    protected function render($page, $plugin)
    {
        $page->getController()->run($page);
    }

    private function invoke_queqe()
    {
        foreach ($this->app->getPluginManager()->getPlugins() as $value) {
            $pages = $value->getPages();
            foreach ($pages as $page) {
                $page->getController()->onCreate();
                if ($page->getController()->onHandleUrl(\Dunp\Request::getParams())) {
                    $this->render($page, $value);
//                    $page->render();
                    return true;
                }
            }
        }
        return false;
    }

    public function load($page)
    {
        try {
            if ($this->invoke_queqe()) {
                return true;
            }else{
                if ($this->load_plugin($page)) {
                    return true;
                }
            }
        } catch (Exception $ex) {
            $this->diplay_error("Error!\n" . $ex->getMessage() . "\n" . $ex->getTraceAsString());
        }
        return false;
    }

    public function load_plugin($page)
    {
        $plugin = null;
        if ($class = $this->getApp()->getPluginManager()->getPage($page, $plugin)) {
            $this->render($class, $plugin);
            return true;
        }
    }

    private function readfile($file)
    {
        if (file_exists($file)) {
            header("Content-Type: " . \Helper::getMimeContentType($file));
            readfile($file);
            return true;
        }
        return false;
    }

    public function file($file)
    {
        if (is_dir($file) || $file == "") {
            if (file_exists($file . "/index.php")) {
                include($file . "/index.php");
                return true;
            }
        } else {
            if (!$this->readfile($file)) {
                $plugin = substr($file, 0, strpos($file, "/"));
                $folder = substr($file, strpos($file, "/") + 1);

                if (substr($folder, 0, strpos($folder, "/")) == "resources") {
                    $filename = $_SERVER['DOCUMENT_ROOT'] . "/../plugins/" . $plugin . "/" . $folder;
                } else {
                    $filename = $_SERVER['DOCUMENT_ROOT'] . "/../plugins/" . $plugin . "/resources/" . $folder;
                }

                if (!$this->readfile($filename)) {
                    foreach ($this->app->getPluginManager()->getPlugins() as $value) {
                        $filename = realpath($value->getPath()) . "/resources/" . $file;
                        if ($this->readfile($filename)) {
                            return true;
                        }
                    }
                } else {
                    return true;
                }
            } else {
                return true;
            }
        }
        return false;
    }

    public function diplay_error($title, $html = true)
    {
        header('HTTP/1.1 404 Not Found');


        if ($html) {
            $template = new Template("bootstrap");
            echo $template->build(new CustomView($this->getApp(), "", "resources/nocontroller.html"));
//            echo "Error loading page. <br>" . $title;
        } else {
            echo "Error loading page. \n" . $title;
        }
    }

    public function getParameter()
    {
        return \Dunp\Request::getParameter();
    }

    public function getParameters()
    {
        return \Dunp\Request::getParameters();
    }

    public function run($scheme)
    {
        if (!$this->file($this->getParameters())) {
            if (!$this->load($this->getParameter())) {
                if($this->getParameter() == "") {
                    if(!$this->load("index"))
                    {
                        $this->diplay_error("File base ('index') not found");
                    }
                }
                else{
                    $this->diplay_error("File base ('{$this->getParameter()}') not found");
                }
            }
        }
    }

}
