<?php

namespace Dunp;

class DefinitionsManager extends Manager
{
    var $app;
    var $path;
    var $file;
    var $data;

    function __construct( $app, $path, $file = "dunp.json")
    {
        $this->path = $path;
        $this->file = $file;
        $this->app = $app;
    }


    public function load()
    {
//        $path, $file = "dunp.json"
        $this->data = (file_get_contents($this->path . "/" . $this->file));
    }
    
    public function get($_config = array())
    {
        global $config;
        $_config['config'] = $config;
        $m = new \Mustache_Engine();
        return json_decode($m->render($this->data, $_config));
    }
}