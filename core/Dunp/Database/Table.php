<?php

namespace Dunp\Database;

use Dunp\Functions;

class Table {

    public $name;

    /**
     *
     * @var Field[]
     */
    public $fields = array();

    /**
     *
     * @var Index[]
     */
    public $indexes = array();

    /**
     * @var Constraint[]
     */
    public $constraints = array();
    
    public $files = array();
    
    public function getHash()
    {
        $hash = "";
        foreach ($this->fields as $key => $value) {
            if(!$value instanceof Field)
            {
                print_r($value);
                echo "<pre>";
                debug_print_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
                die();
            }
            $hash .= $value->getHash();
        }
        foreach ($this->indexes as $key => $value) {
            $hash .= $value->getHash();
        }
        foreach ($this->constraints as $key => $value) {
            $hash .= $value->getHash();
        }
        
        return md5($hash);
    }
    
    public function getCreateTable()
    {
        $sql = "";
        foreach ($this->fields as $field => $value) {
            $sql .= ($sql == "")?"":", ";
            $sql .= $field . " " . $value->type;
            if($value->autoincrement)
            {
                $sql .= " AUTO_INCREMENT PRIMARY KEY";
            }
        }
        
        return "CREATE TABLE " . $this->name . "($sql)";
    }
    
    
    public function getDropTable()
    {
        return "DROP TABLE {$this->name}";
    }

    /**
     * @param $field Field
     */
    public function addField($field)
    {
        if(isset($this->fields[$field->name]))
        {
            Functions::mergeObjects($this->fields[$field->name], $field);
        }
        else{
            $this->fields[$field->name] = $field;
        }
    }

    /**
     * @param $index Index
     */
    public function addIndex($index)
    {
        if(isset($this->indexes[$index->getName()]))
        {
            Functions::mergeObjects($this->indexes[$index->getName()], $index);
        }
        else{
            $this->indexes[$index->getName()] = $index;
        }
    }

    /**
     * @param $constraint Constraint
     */
    public function addConstraint($constraint)
    {
        $key = $constraint->getName();
        if(isset($this->constraints[$key]))
        {
            Functions::mergeObjects($this->constraints[$key], $constraint);
        }
        else{
            $this->constraints[$key] = $constraint;
        }
    }

}