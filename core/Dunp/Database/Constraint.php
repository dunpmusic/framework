<?php
/**
 * Created by PhpStorm.
 * User: manus
 * Date: 3/6/15
 * Time: 23:01
 */

namespace Dunp\Database;


class Constraint extends Comparable {
    var $field;
    var $foreignTable;
    var $foreignField;
    var $onDelete = "CASCADE";
    var $onUpdate = "CASCADE";


    public function getName()
    {
        return $this->field . "_" . $this->foreignTable . "_" . $this->foreignField;
    }

    public function getHash()
    {
        return $this->getName();
    }

    public function getAlterChange($table)
    {
        // TODO: Implement getAlterChange() method.
        return "Constraint::getAlterChange($table)";
    }

    public function getAlterAdd($table)
    {
        // TODO: Implement getAlterAdd() method.
        return "Constraint::getAlterAdd($table)";
    }

    public function getAlterDrop($table)
    {
        // TODO: Implement getAlterDrop() method.
        return "Constraint::getAlterDrop($table)";
    }

} 