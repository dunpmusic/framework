<?php
/**
 * Created by PhpStorm.
 * User: manus
 * Date: 2/6/15
 * Time: 23:02
 */

namespace Dunp\Database;


use Dunp\Log;

class MysqlConnection extends DatabaseConnection {

    /**
     * @var \mysqli
     */
    private $link;
    public function connect()
    {
        try
        {
            global $config;
            // TODO: Implement connect() method.
            $this->link = new \mysqli($config['database.host'],$config['database.user'],$config['database.pass'],$config['database.name']);
            if (!$this->link->connect_errno) {
                $this->setConnected(true);
                return true;
            }

        }
        catch(\Exception $ex)
        {

        }
        return false;
    }

    public function close()
    {
        // TODO: Implement close() method.
        $this->link->close();
    }

    public function query($sql)
    {
        // TODO: Implement select() method.
        $result = $this->exec($sql);
        if($result && $result instanceof \mysqli_result)
        {

            $list = array();
            while($row = $result->fetch_array())
            {
                $list[] = $row;
            }
            return $list;
        }
        return false;
    }

    public function exec($sql)
    {
        // TODO: Implement exec() method.
        $res = $this->link->query($sql);
        if(!$res)
        {
            $this->addError($this->link->error);
        }
        return $res;
    }


    public function getLastInsertId()
    {
        // TODO: Implement getLastInsertId() method.
        return $this->link->insert_id;
    }

    public function escape($data, $forceType = DATABASE_CONNECTION_FORCE_TYPE_NONE)
    {
        // TODO: Implement escape() method.
        return "'" . $this->link->escape_string($data) . "'";
    }

}