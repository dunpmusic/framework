<?php
/**
 * Created by PhpStorm.
 * User: manus
 * Date: 2/6/15
 * Time: 23:02
 */

namespace Dunp\Database;


use Dunp\Log;
define("DATABASE_CONNECTION_FORCE_TYPE_NONE", 0);
define("DATABASE_CONNECTION_FORCE_TYPE_STRING", 1);
define("DATABASE_CONNECTION_FORCE_TYPE_NUMBER", 2);
define("DATABASE_CONNECTION_FORCE_TYPE_DATE", 3);
abstract class DatabaseConnection {

    private $connected = false;
    private static $errors = array();

    public abstract function connect();
    public abstract function close();
    public abstract function query($sql);
    public abstract function exec($sql);
    public abstract function escape($data, $forceType = DATABASE_CONNECTION_FORCE_TYPE_NONE);
    public abstract function getLastInsertId();

    /**
     * @return mixed
     */
    public function isConnected()
    {
        return $this->connected;
    }

    /**
     * @param mixed $connected
     */
    public function setConnected($connected)
    {
        $this->connected = $connected;
    }
    public function getDatabaseName()
    {
        global $config;
        return $config['database.name'];
    }

    public function addError($error)
    {
        self::$errors[] = $error;
        Log::e($error);
    }

    public function getErrors()
    {
        return self::$errors;
    }
}