<?php
/**
 * Created by PhpStorm.
 * User: manus
 * Date: 3/6/15
 * Time: 15:57
 */

namespace Dunp\Database\Cache;


class MemcacheCache extends DatabaseCache {

    /**
     * @var \Memcache
     */
    var $memcache;
    public function connect()
    {
        global $config;
        // TODO: Implement connect() method.
        $this->memcache = new \Memcache();
        $this->memcache->connect($config['memcache']['host'],$config['memcache']['port']);
    }

    public function set($key, $value, $expiration = DATABASE_CACHE_DEFAULT_EXPIRATION)
    {
        // TODO: Implement set() method.
    }

    public function get($key, $default = null)
    {
        // TODO: Implement get() method.
    }

    public function remove($key)
    {
        // TODO: Implement remove() method.
    }
}