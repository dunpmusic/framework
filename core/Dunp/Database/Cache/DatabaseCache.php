<?php
/**
 * Created by PhpStorm.
 * User: manus
 * Date: 3/6/15
 * Time: 15:55
 */
namespace Dunp\Database\Cache;
use Dunp\Log;

define("DATABASE_CACHE_DEFAULT_EXPIRATION", 60);
abstract class DatabaseCache {
    private static $engine;
    public abstract function connect();
    public abstract function set($key, $value, $expiration = DATABASE_CACHE_DEFAULT_EXPIRATION);
    public abstract function get($key, $default = null);
    public abstract function remove($key);


    /**
     * @return DatabaseCache
     */
    public static function getCache()
    {
        global $config;
        if(DatabaseCache::$engine == null)
        {
            if($config['database.memcached'] && class_exists("\\Memcache"))
            {
                DatabaseCache::$engine = new MemcacheCache();
            }
            else{
                DatabaseCache::$engine = new JSONCache();
            }
            Log::d("Using Cache System: " . get_class(self::$engine));
        }
        return DatabaseCache::$engine;

    }

} 