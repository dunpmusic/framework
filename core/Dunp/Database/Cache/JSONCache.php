<?php
/**
 * Created by PhpStorm.
 * User: manus
 * Date: 3/6/15
 * Time: 16:04
 */

namespace Dunp\Database\Cache;


use Dunp\Functions;

class JSONCache extends DatabaseCache {

    var $folder;
    public function connect()
    {
        // TODO: Implement connect() method.
        $this->folder = Functions::getFilePath("cache");
        if(!is_dir($this->folder))
        {
            mkdir($this->folder);
        }
    }

    public function set($key, $value, $expiration = DATABASE_CACHE_DEFAULT_EXPIRATION)
    {
        // TODO: Implement set() method.
        $json['time'] = time() + $expiration;
        $json['data'] = serialize($value);
        return file_put_contents($this->getFile($key), json_encode($json));
    }

    public function get($key, $default = NULL)
    {
        if(file_exists($this->getFile($key)))
        {
            $data = json_decode(file_get_contents($this->getFile($key)));
            if($data->time > time())
            {
                return unserialize($data->data);
            }
        }
        return $default;
    }

    public function remove($key)
    {
        // TODO: Implement remove() method.
        if(file_exists($this->getFile($key)))
        {
            unlink($this->getFile($key));
        }
    }

    private function getFile($key)
    {
        return $this->folder . "/$key.json";
    }
}