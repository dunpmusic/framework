<?php

namespace Dunp\Database;

class Index extends Comparable {

    var $name;
    var $type; // (INDEX, UNIQUE,...)
    var $field;


    public function getName()
    {
        return $this->field . "_" . $this->type;
    }

    public function getHash()
    {
        return $this->getName();
    }

    public function getAlterChange($table)
    {
        // TODO: Implement getAlterChange() method.
        return "ALTER TABLE $table DROP INDEX {$this->getName()}, ADD INDEX {$this->getName()} ($this->field)";
//        return "Index::getAlterChange($table)";
    }

    public function getAlterAdd($table)
    {
        // TODO: Implement getAlterAdd() method.
        return "ALTER TABLE $table ADD INDEX {$this->getName()} ($this->field)";
//        return "Index::getAlterAdd($table)";
    }

    public function getAlterDrop($table)
    {
        // TODO: Implement getAlterDrop() method.
        return "ALTER TABLE $table DROP INDEX {$this->getName()}";
//        return "Index::getAlterDrop($table)";
    }
}