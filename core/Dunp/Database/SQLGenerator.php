<?php

namespace Dunp\Database;

define("SANATIZE_MODE_CAMELCASE", 1);

class SQLGenerator {

    private $folders = array();
    static $database_structure;
    public function addSourcesFolder($folder) {
        if(is_array($folder))
            foreach ($folder as $value)
                $this->folders[] = $value;
        else
            $this->folders[] = $folder;
    }

    /**
     * 
     * @return Table
     */
    public function generate() {
        $jsons = array();
        foreach ($this->folders as $value) {
            $jsons = array_merge($jsons, $this->recursiveList($value));
        }
        $tables = array();
        foreach ($jsons as $file) {
            if(strrpos($file, ".json") == strlen($file) - 5)
            {
                $json = json_decode(file_get_contents($file));
                if($json instanceof \stdClass)
                {
                    $json = array($json);
                }
                if ($json !== NULL && is_array($json)) {
                        $tables = $this->processTables($json, $tables, $file);
                } else {
                    error_log($file . " has a bad JSON format");
                }
            }
        }
        return $tables;
    }

    /**
     * @param $table Table
     * @param $tables Table[]
     * @param null $file
     */
    private function addTable($table, $tables, $file = null) {
        if(isset($table->name))
        {
            $table->files = array(realpath($file));
            if (isset($table->struct))
                $table->fields = $this->processFields($table->struct);
            if (isset($table->indexes))
                $table->indexes = $this->processIndexes($table->indexes);
            if (!isset($tables[$table->name])) {
                $tables[$table->name] = $table;
            } else {
                $tables[$table->name]->fields = array_merge($tables[$table->name]->fields, $table->fields);
                $tables[$table->name]->indexes = array_merge($tables[$table->name]->indexes, $table->indexes);
                $tables[$table->name]->files = array_merge($tables[$table->name]->files, $table->files);
            }
        }
        return $tables;
    }
    /**
     * 
     * @param type $json
     * @return \Table
     */
    private function processTables($json, $tables, $file = null) {
        if($json)
        {

            foreach ($json as $item) {
                $table = new Table();
                if(isset($item->table))
                {
                    $table->name = ($item->table);
                    $table->files = array(realpath($file));
                    if (isset($item->struct))
                        $table->fields = $this->processFields($item->struct);
                    if (isset($item->indexes))
                        $table->indexes = $this->processIndexes($item->indexes);
                    if (isset($item->constraints))
                        $table->constraints = $this->processConstraints($item->constraints);
                    if (!isset($tables[$table->name])) {
                        $tables[$table->name] = $table;
                    } else {
                        $tables[$table->name]->fields = array_merge($tables[$table->name]->fields, $table->fields);
                        $tables[$table->name]->indexes = array_merge($tables[$table->name]->indexes, $table->indexes);
                        $tables[$table->name]->constraints = array_merge($tables[$table->name]->constraints, $table->constraints);
                        $tables[$table->name]->files = array_merge($tables[$table->name]->files, $table->files);
                    }
                }
            }
            return $tables;
        }
    }

    /**
     * 
     * @param type $fields
     * @return Field[]
     */
    private function processFields($fields) {
        if (!is_array($fields))
            return array();
        $items = array();
        foreach ($fields as $item) {
            $field = new Field();
            $field->name = $this->sanitizeText($item->field);
            $field->type = $item->type;
            $field->autoincrement = isset($item->autoincrement) && $item->autoincrement;
            $field->input = isset($item->input) && $item->input;
            $field->hash = isset($item->hash) ? $item->hash : false;
            if(isset($item->null) && $item->null !== NULL)
            {
                $field->null = $item->null;
            }
            if(isset($item->default))
            {
                $field->default = $item->default;
            }
            $items[$field->name] = $field;
        }
        return $items;
    }

    private function processIndexes($indexes) {
        if (!is_array($indexes))
            return array();
        $items = array();
        foreach ($indexes as $item) {
            $index = new Index();
            $index->type = $item->type;
            $index->field = $this->sanitizeText($item->field);
            $items[] = $index;
        }
        return $items;
    }
    private function processConstraints($constraints) {
        if (!is_array($constraints))
            return array();
        $items = array();
        foreach ($constraints as $item) {
            $constraint = new Constraint();
            $constraint->field = $item->field;
            $constraint->foreignField = $item->foreignField;
            $constraint->foreignTable = $item->foreignTable;
            $items[] = $constraint;
        }
        return $items;
    }

    public function sanitizeText($text, $mode = SANATIZE_MODE_CAMELCASE) {
        return $this->toCamelCase($text);

//        return $text;
    }

    private function recursiveList($folder, $depth = 512) {
        if ($depth < 0)
            return array();
        $files = array();
        if(is_dir($folder))
        {
            $items = scandir($folder);
            foreach ($items as $value) {
                if ($value != "." && $value != "..") {
                    $file = $folder . "/" . $value;
                    if (is_dir($file)) {
                        $files = array_merge($files, $this->recursiveList($file, $depth - 1));
                    } else {
                        $files[] = $file;
                    }
                }
            }
        }
        else if(is_file($folder))
        {
            $files[] = $folder;
        }
        return $files;
    }

    /**
     * Translates a string with underscores
     * into camel case (e.g. first_name -> firstName)
     *
     * @param string $str String in underscore format
     * @param bool $capitalise_first_char If true, capitalise the first char in $str
     * @return string $str translated into camel caps
     */
    private function toCamelCase($str, $capitalise_first_char = false) {
        if ($capitalise_first_char) {
            $str[0] = strtoupper($str[0]);
        }
        $func = create_function('$c', 'return strtoupper($c[1]);');
        return preg_replace_callback('/[ ]([a-zA-Z0-9])/', $func, $str);
    }


    public function getDatabaseJSONStructure($table_name = "")
    {
        $db = DatabaseController::getConnection();
        $q = $db->query("SHOW TABLES LIKE '$table_name'");
        foreach ($q as $row) {
            $tables[] = $row[0];
        }
        $json = array();
        foreach ($tables as $table) {
            $q = $db->query("DESCRIBE $table");
            $fields = array();
            foreach ($q as $row) {
                $field = array();
                $field['field'] = $this->sanitizeText( $row['Field'] );
                $field['type'] = $row['Type'];
//                . (($row['Null'] == "NO") ? " NOT NULL" : "") .
//
//                    (($row['Default'] == "") ? "" : " DEFAULT '" . $row['Default'] . "'") .
//                    ""; //$row['Extra'];
                $field['default'] = $row['Default'];
                $field['null'] = $row['Null'] == "YES";
                if($row['Extra'] == 'auto_increment')
                {
                    $field['autoincrement'] = true;
                }
                //                        $field['extra'] = $row['Extra'];
                $fields[] = $field;
            }
            $q = $db->query("select *
from INFORMATION_SCHEMA.STATISTICS as t1 join INFORMATION_SCHEMA.TABLE_CONSTRAINTS as t2 ON t1.INDEX_NAME = t2.`CONSTRAINT_NAME` and t2.table_name = t1.table_name and t2.table_schema = t1.table_schema and t1.TABLE_NAME = '$table' and t1.table_schema = '{$db->getDatabaseName()}'");
            $indexes = array();
            foreach($q as $row)
            {
                $index = array();
                $index['field'] = $row['COLUMN_NAME'];
                $index['type'] = $this->parseIndexType($row['CONSTRAINT_TYPE']);
                $indexes[] = $index;
            }
            $q = $db->query("select *
from information_schema.key_column_usage
where constraint_schema = '{$db->getDatabaseName()}' and referenced_table_schema != '' AND TABLE_NAME = '$table' ");
            $constraints = array();
            foreach($q as $row)
            {

                $constraint = array();
                $constraint['field'] = $row['COLUMN_NAME'];
                $constraint['foreignTable'] = $row['REFERENCED_TABLE_NAME'];
                $constraint['foreignField'] = $row['REFERENCED_COLUMN_NAME'];
                $constraints[] = $constraint;
            }
            $item['table'] = $this->sanitizeText($table);
            $item['struct'] = $fields;
            $item['indexes'] = $indexes;
            $item['constraints'] = $constraints;
            $json[] = $item;
        }
        return $json;
    }
    private function parseIndexType($index)
    {
        return str_replace(" KEY", "", $index);
    }
    /**
     * @return Table[]
     */
    public function getDatabaseStructure($table_name = "")
    {
        return $this->processTables(json_decode( json_encode($this->getDatabaseJSONStructure($table_name))), array());
    }

    /**
     * @param $excepted Table[]
     * @param $current Table[]
     * @param bool $drop
     * @return array
     */
    public function getStructureDifferences($excepted, $current, $drop = false)
    {
        $actions = array();
        if($excepted instanceof Table)
        {
            $excepted = array($excepted);
        }
        foreach($excepted as  $table => $value)
        {
            if(isset($current[$table]))
            {
                $dbTable =  $current[$table];
                if($value->getHash() != $dbTable->getHash())
                {
                    $subject1 = array($value->fields, $value->indexes, $value->constraints);
                    $subject2 = array($dbTable->fields, $dbTable->indexes, $dbTable->constraints);

                    foreach($subject1 as $index => $test1 )
                    {
                        $test2 = $subject2[$index];
                        // Search for add and changes
                        foreach($test1 as $key => $item)
                        {
                            if(isset($test2[$key]))
                            {
                                $item2 = $test2[$key];
                                // possible changes
                                if($item->getHash() != $item2->getHash() )
                                {
                                    if($item instanceof Field)
                                    {
                                        if($item->getLength() > $item2->getLength())
                                        {
                                            $actions[] = array("sql" => $item->getAlterChange($table), "action" => "alter_field");
                                        }
                                    }
                                    else
                                    {
                                        $actions[] = array("sql" => $item->getAlterChange($table), "action" => "alter_field");
                                    }
                                }
                            }
                            else
                            {
                                // create the new instance
                                $actions[] = array("sql" => $item->getAlterAdd($table), "action" => "add_field");
                            }
                        }
                        //Search for deletion
                        foreach ($test2 as $key => $item) {
                            if(!isset($test1[$key]))
                            {
                                if($drop)
                                {
                                    $actions[] = array("sql" => $item->getAlterDrop($table, $key), "action" => "drop_field");
                                }
                            }
                        }
                    }
                }
            }
            else
            {
//                if(!$value instanceof \stdClass) die_stack_trace();
                $actions[] = array("sql" => $value->getCreateTable(), "action" => "create_table");
            }
        }
        if($current)
        {

            foreach ($current as $table => $value) {
                if(!isset($excepted[$table]))
                {
                    if($drop)
                    {
                        $actions[] = array("sql" => $value->getDropTable(), "action" => "drop_table");
                    }
                }
            }
        }
        return $actions;
    }

}
