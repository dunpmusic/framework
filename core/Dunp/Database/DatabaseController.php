<?php

namespace Dunp\Database;

use Dunp\Database\Cache\DatabaseCache;
use Dunp\Functions;
use Dunp\Manager;
use Dunp\Model\JSONModel;
use Dunp\Model\Model;

class DatabaseController extends Manager
{
    
    /**
     *
     * @var \Dunp\WebApp 
     */
    var $app;
    
    /**
     *
     * @var DatabaseConnection
     */
    static $connection;
    /**
     * 
     * @param \Dunp\WebApp $app
     */
    public function __construct($app) {
        $this->app = $app;
        spl_autoload_register(function($model)
        {
            $plugins = $this->app->getPluginManager()->getPlugins();
            foreach($plugins as $plugin)
            {
                $file = $plugin->getPath() . "/model/$model.php";
                if(file_exists($file))
                {
                    require_once $file;
                    return true;
                }
            }
            return false;
        });
    }
    /**
     *
     * @var Table
     */
    private static $struct = null;
    
    public function connect()
    {
        self::$connection = new MysqlConnection();
        if( self::$connection->connect() )
        {
            DatabaseCache::getCache()->connect();
            $file = Functions::getFilePath("databaseLastCheck.time");
            $time = file_get_contents($file);
            if(!$time)
            {
                $time = 0;
            }
            if(time() - $time > 60 * 60)
            {
                file_put_contents($file, time());
                $this->checkDatabase();
            }
            return true;
        }
        return false;
    }

    public function load()
    {
        $this->connect();
    }
    /**
     * 
     * @return Table
     */
    public function loadDatabaseStructure()
    {
        if(self::$struct == null)
        {
            self::$struct = $this->generateStructure();
        }
        return self::$struct;
    }
    public function generateStructure($folders = array())
    {
        $generator = new \Dunp\Database\SQLGenerator();
        $generator->addSourcesFolder($this->getStructureFolders());
        return $generator->generate();
    }
    
    public function getStructureFolders()
    {
        $plugins = $this->app->getPluginManager()->getPlugins();
        $folders = array();
        foreach ($plugins as $plugin) {
            $folders[] = $plugin->getPath() . "/model";
        }
        $folders[] = "struct";
        return $folders;
    }
    
    public function debugStruct()
    {
        $struct = $this->loadDatabaseStructure();
        foreach ($struct as $table => $value) {
            echo "$table\n";
            foreach ($value->files as $file) {
                echo "-> $file\n";
            }
            echo "===================\n";
        }
    }

    /**
     * @deprecated
     * @param bool $remove
     * @return array|bool
     *
     */
    public function checkDatabase($remove = false)
    {
        $generator = new SQLGenerator();
        $excepted = $this->loadDatabaseStructure();
        $current = $generator->getDatabaseStructure();
        $actions = $generator->getStructureDifferences($excepted, $current, $remove);
        $db = self::getConnection();
        foreach($actions as $sql)
        {
            $db->exec($sql['sql']);
        }
        return true;
    }
    
    /**
     * 
     * @param type $name
     * @return Model|JSONModel
     * @throws \RuntimeException
     */
    public function getTable($name)
    {
        $plugins = $this->app->getPluginManager()->getPlugins();
        $plugins = array_reverse($plugins);
        foreach($plugins as $plugin)
        {
            if($model = $plugin->getModel($name))
            {
                return $model;
            }
        }
        $structure = $this->loadDatabaseStructure();
        if(isset($structure[$name]))
        {
            return new JSONModel($name, $structure[$name]);
        }
        throw new \RuntimeException("Table $name not found in structure");
    }

    /**
     * @return DatabaseConnection
     */
    public static function getConnection()
    {
        return self::$connection;
    }
    
    
}