<?php
/**
 * Created by PhpStorm.
 * User: manus
 * Date: 3/6/15
 * Time: 23:39
 */

namespace Dunp\Database;


abstract class Comparable {
    public abstract function getHash();
    public abstract function getAlterChange($table);
    public abstract function getAlterAdd($table);
    public abstract function getAlterDrop($table);
} 