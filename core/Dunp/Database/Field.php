<?php

namespace Dunp\Database;

class Field extends Comparable {

    var $name;
    var $type;
    var $autoincrement;
    var $input;
    var $hash;
    var $null = true;
    var $default;
    
    public function getLength()
    {
        if(($i = strpos($this->type, "(")) !== FALSE)
        {
            $j = strpos($this->type, ")", $i);
            
            return ( substr($this->type, $i + 1, $j - $i - 1 ));
        }
    }
    
    public function getHash()
    {
        return md5($this->hash($this->name) . $this->hash($this->type) . $this->hash($this->autoincrement));
    }

    private function hash($data)
    {
        return str_replace(" ", "", trim( $data));
    }
    
    public function getType()
    {
        $sql = $this->type;
        if(!$this->null)
        {
            $sql .= " NOT NULL";
        }
        if($this->default)
        {
            $sql .= " DEFAULT '$this->default'";
        }
        if($this->autoincrement)
        {
            $sql .= " AUTO_INCREMENT";
        }
        return $sql;
    }
    public function getAlterAdd($table)
    {
        return "ALTER TABLE $table ADD {$this->name} {$this->getType()}";
    }
    public function getAlterDrop($table, $name = null)
    {
        if($name == null)
        {
            $name = $this->name;
        }
        return "ALTER TABLE $table DROP COLUMN {$name}";
    }
    public function getAlterChange($table)
    {
        //ALTER TABLE  `clients` CHANGE  `id`  `id` INT( 11 ) NOT NULL AUTO_INCREMENT ;

        return "ALTER TABLE $table CHANGE {$this->name} {$this->name} {$this->getType()}";
    }

}