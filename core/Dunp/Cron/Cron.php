<?php
/**
 * Created by PhpStorm.
 * User: manus
 * Date: 12/6/15
 * Time: 9:36
 */

namespace Dunp\Cron;


use Dunp\WebApp;

abstract class Cron {


    /**
     * @var WebApp
     */
    private $app;

    function __construct($app)
    {
        $this->app = $app;
    }



    /**
     * @param $args,...
     * @return mixed
     */
    public abstract function exec($args = "");

    /**
     * @return WebApp
     */
    public function getApp()
    {
        return $this->app;
    }



} 