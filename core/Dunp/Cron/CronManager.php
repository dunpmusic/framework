<?php
/**
 * Created by PhpStorm.
 * User: manus
 * Date: 5/6/15
 * Time: 15:12
 */

namespace Dunp\Cron;

use Dunp\Manager;

class CronManager extends Manager {


    public function load()
    {
        $plugins = $this->app->getPluginManager()->getPlugins();
        $list = [];
        foreach($plugins as $plugin)
        {
            $jobs = $plugin->getCronJobs();
            foreach($jobs as $job)
            {
                $this->append(array($job->getCommand()));
                $list[] = $job->getPlugin()->getNamespace() . "::" . $job->getTask();
            }
        }

        foreach($this->listJobs() as $job)
        {

        }
    }
    /**
     * @var \Dunp\WebApp
     */
    var $app;
    var $file;

    /**
     * @param $app WebApp
     */
    function __construct($app)
    {
        parent::__construct($app);
        $this->app = $app;
    }

    /**
     * @param $task \cron_tasks
     */
    public function addCronTask($task)
    {
        if(!$task = $task->selectByFunction($task->function))
        {

        }
    }

//    public function addCronTask($function, $args = array(), $plugin = null, $minute = "*", $hour = "*", $day = "*", $month = "*", $week = "*")
//    {
//        $model = new \cron_tasks();
//        if(!$model = $model->selectByFunction($function))
//        {
//            $model = new \cron_tasks();
//        }
//        $model->cron = "$minute $hour $day $month $week";
//        $model->args = serialize($args);
//        $model->function = $function;
//        $model->namespace = $plugin;
//        if($model->save())
//        {
//            if(!$model->id)
//            {
//                $model->id = $model->getDb()->getLastInsertId();
//            }
//            $this->append(array($model->getCommand()));
//            return true;
//        }
//        return false;
//
//    }
//
//
//    public function removeCronTask($function)
//    {
//
//        $model = new cron_tasks();
//        $model = $model->selectByFunction($function);
//        if($model)
//        {
//            $model->delete();
//        }
//    }

    public function append($jobs)
    {
        foreach($jobs as $job)
        {
            exec( '( crontab -l | grep -v "' . $job  . '" ; echo "' . $job  . '" ) | crontab -' );
        }
    }
    public function remove($jobs)
    {
        foreach($jobs as $job)
        {
            exec( '( crontab -l | grep -v "' . $job  . '" ) | crontab -' );
        }
    }


    public function listJobs()
    {
        return explode("\n", exec("crontab -l"));
    }
}