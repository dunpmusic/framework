<?php
/**
 * Created by PhpStorm.
 * User: manus
 * Date: 14/6/15
 * Time: 10:51
 */

namespace Dunp\Cron;


use Dunp\Plugin\Plugin;

class CronTask {

    private $task;
    /**
     * @var Plugin
     */
    private $plugin;
    private $cron;
    private $args;

    /**
     * @return mixed
     */
    public function getCron()
    {
        return $this->cron;
    }

    /**
     * @param mixed $cron
     */
    public function setCron($cron)
    {
        $this->cron = $cron;
    }

    /**
     * @return Plugin
     */
    public function getPlugin()
    {
        return $this->plugin;
    }

    /**
     * @param Plugin $namespace
     */
    public function setPlugin($namespace)
    {
        $this->plugin = $namespace;
    }

    /**
     * @return mixed
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param mixed $task
     */
    public function setTask($task)
    {
        $this->task = $task;
    }

    /**
     * @return mixed
     */
    public function getArgs()
    {
        return $this->args;
    }

    /**
     * @param mixed $args
     */
    public function setArgs($args)
    {
        $this->args = $args;
    }


    public function getCommand()
    {
        $path = realpath(__DIR__ . "/../../../dunp");
        return $this->cron . " php $path cron '{$this->plugin->getNamespace()}::{$this->task}'";
    }





} 