<?php

namespace Dunp;

abstract class View {

    /**
     *
     * @var Controller
     */
    private $controller;
    private $pagename;
    private $basedir;
    private $theme;
    private $template = "";
    private $template_data = array();
    private $filters;

    public function __construct($app, $basedir) {
        $name = get_class($this);
        if(strpos($name, "\\") !== FALSE)
        {
            $cname = (substr($name,0, strpos($name, "\\")));
            if(strrpos($cname, "_") !== FALSE)
            {
                $cname = substr($cname,strrpos($cname, "_") + 1);

                if($cname == "plugin")
                {
                    $name = substr($name,strpos($name, "\\") + 1);

                }
            }
        }
        $this->pagename = (substr($name, 0, strrpos($name, "_")));
        $this->basedir = $basedir;
        $this->controller = $this->setupController($app);
        $this->template = $this->pagename;
    }

    private function getInternalNamespace()
    {
        $name = get_class($this);
        if(strpos($name, "\\") !== FALSE)
        {
            $cname = (substr($name,0, strpos($name, "\\")));
            if(strrpos($cname, "_") !== FALSE)
            {
                $cname = substr($cname,strrpos($cname, "_") + 1);

                if($cname == "plugin")
                {
                    return substr($name,0, strpos($name, "\\") + 1);

                }
            }
        }
        return "";
    }

    public function getPageName() {
        return $this->pagename;
    }

    public function getViewFile() {
        return "pages/views/" . $this->getPageName() . ".html";
    }

    /**
     * 
     * @return Controller
     */
    public function getController() {
        return $this->controller;
    }

    /**
     * @return Controller Description
     */
    private function setupController($app) {
        $classname = $this->getPageName() . "_controller";
        $names = array($classname, $this->getInternalNamespace() . $classname);

        foreach($names as $name)
        {
            if (class_exists($name)) {
                return new $name($app, $this->pagename, $this->basedir);
            }
        }
        return new Controller($app, $this->pagename, $this->basedir);

    }

    public abstract function display();

    public function render($tpl = null, $data = array()) {

        $app = $this->getController()->getApp();
        if($tpl != null){
            $this->setTemplate($tpl);
        }
        $this->template_data = $data;
        if($this->theme == '')
        {
            $theme = $app->getDefaultTheme();
        }
        else
        {
            $theme = $this->getTheme();
        }
        echo $this->build($theme, $app->getDefaultThemePath($theme));
    }

    protected function build($theme, $themePath) {
        $template = new View\Template($theme, $themePath);
        // Load controller
        return $template->build($this);
    }

    public function __destruct() {
        if($this->controller)
            $this->controller->onStop();
    }

    public function getStyles() {
        return array();
    }

    public function getBaseDir() {
        return $this->basedir;
    }

    public function getSectionPath($file) {
        return $this->basedir . "/../view/section/$file";
    }
    public function getIncludePath($file) {
        return $this->basedir . "/view/$file";
    }

    public function getRequestParameter($index = 0) {
        return Request::getParameter($index);
    }

    public function getRequestParameters() {
        return Request::getParameters();
    }

    public function getCustomContent() {
        return null;
    }

    /**
     * @return mixed
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * @param mixed $theme
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;
    }

    /**
     * @return mixed
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param mixed $template
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }

    public function addFilter($name, $callable)
    {
        $this->filters[$name] = $callable;
    }

    /**
     * @return mixed
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * @return array
     */
    public function getTemplateData()
    {
        return $this->template_data;
    }











}
