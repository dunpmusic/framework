<?php

namespace Dunp\Model;
use Dunp\Database\Cache\DatabaseCache;
use Dunp\Database\DatabaseConnection;
use Dunp\Database\DatabaseController;
use Dunp\Database\SQLGenerator;
use Dunp\Database\Table;
use Dunp\Functions;
use Dunp\Log;
use Dunp\Model\MysqlModel;

abstract class Model
{

    /**
     * @var DatabaseConnection
     */
    private $db;
    private $cache;
    var $tableName;
    /**
     * @var Table[]
     */
    var $structure;
    private $errors = array();

    /**
     * @param $table string
     * @param $structure Table
     */
    public function __construct($table, $structure) {
        $this->db = DatabaseController::getConnection();
        $this->cache = DatabaseCache::getCache();
        $this->tableName = $table;
        $this->structure = $structure;

        $this->checkStructure();
    }

    public abstract function install();
    public abstract function save();
    public abstract function exists();
    public abstract function delete();

    /**
     * @return DatabaseConnection
     */
    public function getDb()
    {
        return $this->db;
    }

    /**
     * @return DatabaseCache
     */
    public function getCache()
    {
        return $this->cache;
    }

    /**
     * @return mixed
     */
    public function getStructure()
    {
        return $this->structure;
    }

    /**
     * @return mixed
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    private function checkStructure()
    {
        $key = "last_model_check_" . $this->tableName;
        $time = $this->getCache()->get($key , 0);
        if(time() - $time > 0)
        {
            $this->getCache()->set($key, time());
            Log::d("Checking table $time {$this->tableName}");
            $sql = new SQLGenerator();
            $actions = $sql->getStructureDifferences($this->structure, $sql->getDatabaseStructure($this->tableName));
            foreach($actions as $action)
            {
                Log::d("Action: " . $action['sql']);
                if($this->db->exec($action['sql']))
                {
                    if($action['action'] == "create_table")
                    {
                        $this->install();
                    }
                }
                else{
//                    print_r_die($this->db->getErrors());
                }
            }
        }

    }

    public function getLastInsertId()
    {
        return $this->db->getLastInsertId();
    }

    public function addError($error)
    {
        $this->errors[] = Functions::getErrorTrace($error);
    }

    public function getErrors()
    {
        return $this->errors;
    }
}