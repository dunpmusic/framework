<?php
namespace Dunp\Email;

use Dunp\View\TemplateData;

class EmailTemplate
{

    private $template;
    private $headers = array();

    function __construct($template, $from = "Dunp Framework <framework@dunpmusic.com>")
    {
        $this->template = $template;
        $this->setHeader("Content-type", "text/html");
        $this->setHeader("From", $from);
    }
    public function setHeader($key, $value) {
        $this->headers[$key] = $value;
    }

    public function setHeaders($headers) {
        $this->headers = $headers;
    }


    public function getHeaders() {
        return $this->headers;
    }

    public function send($emails, $subject, $content, $template_data = array()) {
        if (!is_array($emails)) {
            $emails = array($emails);
        }
        //Build headers
        $headers = "";
        foreach ($this->headers as $key => $value) {
            $headers .= ($headers == "") ? "" : "\r\n";
            $headers .= $key . ": " . $value;
        }
        $count = 0;
        //send email..
        foreach ($emails as $value) {

            $template_data['mail'] = array(
                "subject" => $subject,
                "content" => $content,
                "time" => time(),
                "email" => $value,
            );
            $template = new TemplateData();
            $html_content = $template->arr($template_data)->render($this->template);

            if (mail($value, $subject, $html_content, $headers)) {
                $count ++;
            }

        }
        return $count;
    }
}