<?php
/**
 * Created by PhpStorm.
 * User: manus
 * Date: 1/6/15
 * Time: 21:39
 */

namespace Dunp\Http;


use Dunp\Scheme;

class HttpRoute {

    var $method;
    var $query;
    var $callback;

    private $required;
    private $optional;

    function __construct($method = "", $query = "", $callback = null)
    {
        $this->method = $method;
        $this->query = $query;
        $this->callback = $callback;
        $this->parseQuery($query);
    }

    private function parseQuery($query)
    {
        $this->params = array();
        $param = "\/(:?[a-zA-Z0-9%\.\-_:&?=+*$~#@|!]*)"; // Like: "/", "/home", "/settings/audio", "/user/:id"
        $optional = "\($param\)"; // Like: "/user(/:id)/audio"
        preg_match_all("/$param|$optional/", $query, $matches);
        $this->required = $matches[1];
        $this->optional = $matches[2];
    }

    private function getMethodMatch()
    {
        $items = explode("|", strtoupper($this->method));
        return "(" . implode(")|(", $items) . ")";
    }
    /**
     * @param $scheme Scheme
     * @return bool
     */
    public function match($scheme, &$matches = array())
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if(preg_match("/" . $this->getMethodMatch() . "/", $method) >= 1)
        {
            $items = array();
            $items[0] = "\/";
            foreach($this->required as $key => $item)
            {
                if(!empty($item))
                    $items[$key] = $this->buildItem($item);
            }
            foreach($this->optional as $key => $item)
            {
                if(!empty($item))
                    $items[$key] = $this->buildItem($item, true);
            }
            ksort($items);
            $preg = implode("", $items) . "$";
            $url = "/" . implode("/", $scheme->path);
            $res = preg_match_all("/$preg/", $url, $matches);
            if($res === FALSE)
            {
                die("Error: " . $this->preg_errtxt(preg_last_error()));
            }
            else{
                if($res)
                {
                    return true;
                }
            }
        }
        return false;

    }
    private function preg_errtxt($errcode)
    {
        static $errtext;

        if (!isset($errtxt))
        {
            $errtext = array();
            $constants = get_defined_constants(true);
            foreach ($constants['pcre'] as $c => $n) if (preg_match('/_ERROR$/', $c)) $errtext[$n] = $c;
        }

        return array_key_exists($errcode, $errtext)? $errtext[$errcode] : NULL;
    }
    private function buildItem($item, $optional = false)
    {
        if($item != "")
        {
            $urlItems = "[a-zA-Z0-9_.\-~%&=()\[\]]*";
            if(strpos($item, ":") === 0)
            {

                $item = "\/($urlItems)";
            }
            else{
                $item = "\/" . $item . "\b";
            }
            if($optional)
            {
                $item = "(?:" . $item . ")?";
            }
        }
        return $item;
    }


} 