<?php
namespace Dunp\Http;

class HttpRequest
{
    public static function get($url, $headers = false)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, $headers);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}