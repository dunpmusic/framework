<?php

namespace Dunp;

abstract class Component
{
    
    const COMPONENT_LOW_PRIORITY = 100;
    /**
     *
     * @var WebApp
     */
    protected $app;
    /**
     *
     * @var Scheme 
     */
    private $schemes = array();
    private $priority = 0;
    public function __construct($app, $priority = 10) {
        $this->app = $app;
        $this->priority = $priority;
    }
    public abstract function run($scheme);
    
    /**
     * 
     * @return WebApp
     */
    public function getApp()
    {
        return $this->app;
    }
    
    public function addScheme($scheme)
    {
        $this->schemes[] = $scheme;
        return $this;
    }
    
    public function containsScheme($scheme)
    {
        foreach ($this->schemes as $value) {
            if($value->equals($scheme))
            {
                return $value;
            }
        }
        return false;
    }
    
    public function __toString() {
        return get_called_class();
    }

    public function getPriority($base = 0)
    {
        return $this->priority + $base;
    }
    
    
}