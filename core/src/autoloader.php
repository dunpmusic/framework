<?php
//include_once __DIR__ . '/../config/config.php';

spl_autoload_register(function($class)
{
    $extensions = array(".php", ".class.php");
    $folders = array(__DIR__,__DIR__ . "/lists",__DIR__ . "/model");
    
    
    foreach ($folders as $folder) {
        foreach ($extensions as $ext) {
            $filename = $folder . "/" . $class . $ext;
            if(file_exists($filename))
            {
                include_once $filename;
                break;
            }
        }
    }
});
