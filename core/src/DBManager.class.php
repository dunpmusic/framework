<?php

//require_once __DIR__ . '/../config/config.php';
/*
 * Clase encargada de realizar la conexión con la base de datos
 *
 * USAR SIEMPRE: "mysqli_real_escape_string($VAR)"  para proteger de SQL Inyection
 * 
 * Ver docs/dunp.sql para importar la estructura de la base de datos.
 *
 * */
global $config;

define('MEMCACHED_ENABLED', $config['database.memcached']);
define('DEFAULT_IPP', $config['database.ipp']);
/**
 * Manage connection between database and application
 */
class DBManager {
    

    private $MEMCACHE_TIMEOUT = 3600; // 1 Hora
    private $connection;

    private $memcache;
    private $memcache_server = "localhost";
    private $memcache_server_port = 11211;
    private $connected = false;
    private $queries = array();
    //$GLOBALS['number_queries'];
    //$GLOBALS['number_queries_sql'];
    //$GLOBALS['timer_queries'];
    private $err;

    /**
     * Create an instance of DBManager
     */
    function __construct() {
        if (!isset($GLOBALS['number_queries']))
            $GLOBALS['number_queries'] = 0;
        if (!isset($GLOBALS['number_queries_sql']))
            $GLOBALS['number_queries_sql'] = 0;
        if (!isset($GLOBALS['timer_queries']))
            $GLOBALS['timer_queries'] = 0;
        if (!isset($GLOBALS['timer_exec']))
            $GLOBALS['timer_exec'] = microtime();
        if (!isset($GLOBALS['instances']))
            $GLOBALS['instances'] = 0;
        if (!isset($GLOBALS['instances_destruct']))
            $GLOBALS['instances_destruct'] = 0;
        if (!isset($GLOBALS['mysqli_connections']))
            $GLOBALS['mysqli_connections'] = 0;
        if(!isset($GLOBALS['dbmanager']))
        {
            $GLOBALS['dbmanager'] = array();
        }
        $GLOBALS['instances']++;
        //NO HACEN FALTAN, FORMAN PARTE DE DBMANAGER
//        $this->Server	= "localhost";
//        $this->User	= "merkamarket";
//        $this->Password	= "Kzw96Wv5pu9WPeeJ";
//        $this->Database	= "merkamarket";
        if(MEMCACHED_ENABLED)
            $this->memcache = new Memcache;
    }

    function __destruct() {
        //echo "DBManager __destruct\n";
        $GLOBALS['instances_destruct']++;
        $this->close();
        //if($GLOBALS['instances'] == $GLOBALS['instances_destruct'])
         //   echo "/* " . $this->get_result() . " */";
    }
    /**
     * 
     * Start connection to database
     * @global type $config
     * @param type $charset
     * @return boolean
     * @throws Exception
     */
    public function connect($charset = 'utf8') {
        global $config;
        if (!isset($GLOBALS['dbmanager']['connected'])) {
            if (!($con = @mysqli_connect($config['database.host'], $config['database.user'], $config['database.pass'], $config['database.name']))) {
                $this->err = "MySQL connection error";
//                throw new Exception($this->err);
                return false;
            }
//            if (!@mysqli_select_db(, $con)) {
//                $this->err = "MySQL selection error";
//                throw new Exception($this->err);
//                return false;
//            }
            $GLOBALS['dbmanager']['connection'] = $con;
            $GLOBALS['dbmanager']['connected'] = true;
            $GLOBALS['mysqli_connections']++;
            mysqli_set_charset($GLOBALS['dbmanager']['connection'], $charset);
        }
        if (MEMCACHED_ENABLED) {
            if (!$this->memcache->connect($this->memcache_server, $this->memcache_server_port)) {
                $this->err = "MemCache connection error";
                return false;
            }
        }
        return true;
    }
    public function getConfigArray()
    {
        global $config;
        return $config;
    }
    /**
     * Close connection and release data.
     */
    protected function close() {
        for ($i = 0; $i < count($this->queries); $i++) {
            if ($this->queries[$i] !== TRUE && $this->queries[$i] !== FALSE)
            {
                mysqli_free_result($this->queries[$i]);
            }
        }
        if ($GLOBALS['instances_destruct'] == $GLOBALS['instances']) {
            if (isset($GLOBALS['dbmanager']['connection']) && $GLOBALS['dbmanager']['connection'] != NULL)
            {
                mysqli_close($GLOBALS['dbmanager']['connection']);
            }
        }
        if ($this->connected && MEMCACHED_ENABLED)
        {
            $this->memcache->close();
        }
        unset($GLOBALS['dbmanager']['connected']);
    }
    /**
     * Return connection status
     * @return type
     */
    function isConnected() {
        return $GLOBALS['dbmanager']['connected'];
    }
    /**
     * Get last error
     * @return type
     */
    function get_error() {
        return $this->err;
    }
    /**
     * Select paginated data into database. Using pagination_key and page can store into cache.
     * @param type $query
     * @param type $pagination_key
     * @param type $page
     * @return type
     */
    protected function select_paginated($query, $pagination_key = NULL, $page = 0) {
        if (MEMCACHED_ENABLED) {
            $pages_count = $this->memcache->get($pagination_key . "_keys"); // Memcached object
            if ($pages_count) {
                if (!in_array($page, $pages_count)) {
                    $pages_count[] = $page;
                }
            } else {
                $pages_count[] = $page;
            }
            $this->memcache->set($pagination_key . "_keys", $pages_count, MEMCACHE_COMPRESSED, $this->MEMCACHE_TIMEOUT);
        }
        return $this->select($query, $pagination_key . "_" . $page);
    }
    /**
     * Select and cache data referenced by key
     * @param type $query
     * @param type $key
     * @return type
     */
    protected function select($query, $key = NULL) {
        $GLOBALS['number_queries']++;
        $result = array();
        if (MEMCACHED_ENABLED) {

            if ($key == NULL)
                $key = md5($query); // Unique Words
            $cache_result = array();
            $cache_result = $this->memcache->get($key); // Memcached object
            
            if ($cache_result) {
                $result = $cache_result;
            } else {
                // First User Request
                $res = $this->do_mysqli_query($query);
                while ($row = mysqli_fetch_array($res))
                    $result[] = $row; // Results storing in array
                if(count($result) > 0)
                    $this->memcache->set($key, $result, MEMCACHE_COMPRESSED, $this->MEMCACHE_TIMEOUT);
            }
        } else {
            $res = $this->do_mysqli_query($query);
            while ($row = mysqli_fetch_array($res))
                $result[] = $row; // Results storing in array
        }
        return $result;
    }
    /**
     * Execute mysql sta
     * @param type $query
     * @param type $key
     * @return boolean
     * @throws Exception
     */
    protected function update($query, $key = null) {
        $GLOBALS['number_queries']++;
        if (!$this->do_mysqli_query($query))
            throw new Exception(mysqli_error());
        
        if($key != null)
        {
            $this->remove($key);
        }
        return mysqli_affected_rows($GLOBALS['dbmanager']['connection']);
    }

    /**
     * Make a mysql query and returns last insert id
     * @param type $query
     * @return boolean
     */
    protected function insert($query, $key = null) {
        $GLOBALS['number_queries']++;
        if (!($res = $this->do_mysqli_query($query)))
        {
            $this->log_error(mysqli_error() . "\n" . get_call_stack());
            return false;
        }
        if($key != null)
            $this->remove($key);
        $insert = mysqli_insert_id($GLOBALS['dbmanager']['connection']);
        if($insert) return $insert;
        return true;
    }
    
    protected function delete($query, $key = null)
    {
         $GLOBALS['number_queries']++;
        if (!$this->do_mysqli_query($query))
        {
            $this->log_error(mysqli_error() . "\n" . get_call_stack());
            throw new Exception(mysqli_error());
        }
        if($key != null)
            $this->remove($key);
        return true;
    }

    public function remove_paginated($pagination_key) {

        if (MEMCACHED_ENABLED) {
            $pages_count = $this->memcache->get($pagination_key . "_keys"); // Memcached object
            if (!$pages_count)
                return false;
            foreach ($pages_count as $value) {

                $this->remove($pagination_key . "_" . ($value));
            }
            return $this->remove($pagination_key . "_keys");
        }
    }

    protected function get($key) {
        if (MEMCACHED_ENABLED)
            return $this->memcache->get($key);
    }

    protected function set($key, $data) {
        if (MEMCACHED_ENABLED)
            return $this->memcache->set($key, $data, MEMCACHE_COMPRESSED, $this->MEMCACHE_TIMEOUT);
    }

    public function remove($key) {
        if (MEMCACHED_ENABLED)
            return $this->memcache->delete($key);
    }

    public function do_mysqli_query($sql) {
        $GLOBALS['number_queries_sql']++;
        $query_start = microtime(true);
        $query = mysqli_query($GLOBALS['dbmanager']['connection'], $sql);
        $query_ends = microtime(true); // - $query_start;

        $GLOBALS['timer_queries'] += round($query_ends - $query_start, 6);
        if ($query) {
            $this->queries[] = $query;
        } else {
            DBManager::log_error($sql . "\n" . mysqli_error($GLOBALS['dbmanager']['connection']));
            throw new Exception($sql . "\n" . mysqli_error($GLOBALS['dbmanager']['connection']));

        }
        return $query;
    }

    function get_number_of_queries_sql() {
        return $GLOBALS['number_queries_sql'];
    }

    function get_number_of_queries() {
        return $GLOBALS['number_queries'];
    }

    function get_time_of_queries() {
        return $GLOBALS['timer_queries'];
    }

    public static function get_result() {
        return "Realizadas " . $GLOBALS['number_queries'] . " consultas en " . $GLOBALS['timer_queries'] . " segundos, de las cuales " . $GLOBALS['number_queries_sql'] . " han sido a MySql. Generada en " . (microtime() - $GLOBALS['timer_exec']) . " microsegundos. Se han creado: " . $GLOBALS['instances'] . " instancias de DBManager, las cuales han establecido: " . $GLOBALS['mysqli_connections'] . " conexiones a MySQL.";
    }
    
    /**
     * 
     * @param Exception $e
     */
    public static function log_exception($e) {
        error_log($e);
    }
    
    public static function log_error($e) {
        error_log($e);
    }
    /**
     * Parsea directamente una consulta tipo: SELECT count(*) FROM ...
     * @param type $result
     * @return type
     */
    public static function parse_count_query($result)
    {
        if(count($result) == 1)
        {
            return $result[0][0];
        }
        return -1;
    }
}

?>
