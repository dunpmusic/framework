<?php
function print_r_die($data)
{
    echo "<pre>";

    print_r($data);
    die();
}
function print_r_custom($data)
{
    echo "<pre>";

    print_r($data);
}
function die_stack_trace($html = true)
{
    if($html)
    {
        echo "<pre>";
    }
    debug_print_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
    die();
}

function print_r_exclude($info, $return= false)
{
    recursive_unset($info, "strings");

    return print_r($info, $return);
}
function recursive_unset(&$array, $unwanted_key) {
    unset($array[$unwanted_key]);
    foreach ($array as &$value) {
        if (is_array($value)) {
            recursive_unset($value, $unwanted_key);
        }
    }
}
function microtime_float()
{
    list($useg1, $seg1) = explode(" ", microtime());
    return (float)$useg1 + (float)$seg1;
}
function start_timer()
{
    return array("time" => microtime_float());
}

function stop_timer($timer)
{
    return microtime_float() - $timer['time'];
}