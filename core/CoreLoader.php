<?php

class CoreLoader {

    /**
     *
     * @var Dunp\Component 
     */
    var $components = array();

    /**
     *
     * @var Dunp\WebApp 
     */
    var $app;
    /**
     * 
     * @param String $base Root dir
     */
    public function __construct($base = "..") {
        $this->app = new \Dunp\WebApp($base);
    }

    public function addComponent($component) {
        $this->components[] = $component;
        
    }
    
    public function init()
    {
        $this->parse_request();
        $this->getApp()->initManagers();
        $plugins = $this->getApp()->getPluginManager()->getPlugins();
        foreach ($plugins as $plugin) {
            $plugin->onRegisterComponents($this->getApp());
            $components = $plugin->getComponents();
            foreach ($components as $component) {
                $this->addComponent($component);
            }
        }

        return true;
    }

    public function database()
    {
        $this->getApp()->getDatabaseController()->checkDatabase();
    }
    /**
     * 
     * @param Dunp\Component $a
     * @param Dunp\Component $b
     * @return type
     */
    function sort($a, $b) {
        return $a->getPriority() > $b->getPriority();
    }

    public function run($url) {
        $scheme = \Dunp\Scheme::parseUri($url);
        
        usort($this->components, array($this, "sort"));
        foreach ($this->components as $component) {
            if ($current = $component->containsScheme($scheme)) {
                $timer = start_timer();
                \Dunp\Log::d(sprintf("Running %s, using %s", $url, get_class($component)));
                $this->runComponent($component, $current, $scheme);
                \Dunp\Log::d("Component Time: " . stop_timer($timer));
                break;
            }
        }
        return false;
    }

    /**
     * 
     * @param Dunp\Component $component
     * @param Dunp\Scheme $scheme
     * @param Dunp\Scheme $callScheme
     */
    public function runComponent($component, $scheme, $callScheme) {
        global $relative;

        $relative = "";
        foreach ($scheme->path as $value) {
            $relative = "/" . $value;
        }
        $backup = $_SERVER;
        if (isset($_SERVER['PATH_INFO']))
            $_SERVER['PATH_INFO'] = str_replace($relative, "", $_SERVER['PATH_INFO']);
        if (isset($_SERVER['QUERY_STRING']))
            $_SERVER['QUERY_STRING'] = str_replace($relative, "", $_SERVER['QUERY_STRING']);
        if(isset($_SERVER['PHP_SELF']))
            $_SERVER['PHP_SELF'] = str_replace($relative, "", $_SERVER['PHP_SELF']);
        if(isset($_SERVER['REQUEST_URI']))
            $_SERVER['REQUEST_URI'] = str_replace($relative, "", $_SERVER['REQUEST_URI']);

        $component->run($callScheme);
    }

    public function getApp() {
        return $this->app;
    }

    public function error($message) {
        echo "<h1>Error</h1>";
        echo $message;
        die();
    }

    public function exception(Exception $ex) {
        $this->error($this->format_error($ex->getCode(), $ex->getMessage(), $ex->getFile(), $ex->getLine(), $ex->getTrace()));
    }

    public function errorHandler($errno, $errstr, $errfile, $errline) {
        $this->error($this->format_error($errno, $errstr, $errfile, $errline));
    }

    function fatal_handler() {
        $errfile = "unknown file";
        $errstr = "shutdown";
        $errno = E_CORE_ERROR;
        $errline = 0;

        $error = error_get_last();

        $isError = false;

        if ($error) {
            switch ($error['type']) {
                case E_ERROR:
                case E_CORE_ERROR:
                case E_COMPILE_ERROR:
                case E_USER_ERROR:
                    $isError = true;
                    break;
            }
        }

        if ($isError) {

            if ($error !== NULL) {
                $errno = $error["type"];
                $errfile = $error["file"];
                $errline = $error["line"];
                $errstr = $error["message"];

                $this->error($this->format_error($errno, $errstr, $errfile, $errline));
            }
        }
    }

    function format_error($errno, $errstr, $errfile, $errline, $trace = null) {
        if ($trace == null) {
            $trace = debug_backtrace(false);
            unset($trace[0]); //Remove call to this function from stack trace
        }

        $trace = $this->debug_backtrace_string($trace);

        $content = "<table><thead bgcolor='#c8c8c8'><th>Item</th><th>Description</th></thead><tbody>";
        $content .= "<tr valign='top'><td><b>Error</b></td><td><pre>$errstr</pre></td></tr>";
        $content .= "<tr valign='top'><td><b>Errno</b></td><td><pre>$errno</pre></td></tr>";
        $content .= "<tr valign='top'><td><b>File</b></td><td>$errfile</td></tr>";
        $content .= "<tr valign='top'><td><b>Line</b></td><td>$errline</td></tr>";
        $content .= "<tr valign='top'><td><b>Trace</b></td><td><pre>$trace</pre></td></tr>";
        $content .= '</tbody></table>';
        error_log($errstr . " " . $trace);

        return $content;
    }

    function debug_backtrace_string($trace) {
        $stack = '';
        $i = 1;
        foreach($trace as $node) {
            if(isset($node['file'], $node['line'], $node['class']))
            {

                $stack .= "#$i ". (isset($node['file'])?$node['file']:"Unknown") ."(" .$node['line']."): ";
                if(isset($node['class'])) {
                    $stack .= $node['class'] . "->";
                }
                $stack .= $node['function'];

                if(isset($node['args']))
                {
                    $args = "";
                    foreach($node['args'] as $arg)
                    {
                        $args .= ($args == "")?"":", ";
                        $args .= get_class($arg);
                    }
                    $stack .= "(" . $args . ")" . PHP_EOL;
                }
                else{
                    $stack .= "()" . PHP_EOL;
                }
            }
            else
            {
                $stack .= "#$i ". print_r($node, true);
            }
            $i++;
        }
        return $stack;
    }

    function parse_request() {
        if(isset($_SERVER['REQUEST_URI']))
        {
            $pos = strpos($_SERVER['REQUEST_URI'], "?");
            if ($pos > 0) {
                $sub = substr($_SERVER['REQUEST_URI'], $pos + 1);
                //$params = split("&", $sub);
                $params = explode("&", $sub);
                foreach ($params as $param) {
                    $opt = explode("=", $param);
                    $_REQUEST[$opt[0]] = urldecode($opt[1]);
                    $_GET[$opt[0]] = urldecode($opt[1]);
                }
            }
        }
    }

}
