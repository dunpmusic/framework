<?php
/**
 * Created by PhpStorm.
 * User: manus
 * Date: 2/6/15
 * Time: 15:22
 */


class HttpRouteTest extends \PHPUnit_Framework_TestCase {


    public function testParseQuery()
    {
        $patterns = array(
            array("GET", "/"),
            array("GET", "/home"),
            array("GET", "/home/:id"),
//            array("GET", "/home/:id/audio"),
            array("GET", "/home/:id/audio(/:test)"),
            array("GET", "/home/:id/audio(/:test)"),

            array("POST", "/user"),
            array("DELETE", "/user/:id"),
            array("GET", "/user(/:id)"),
            array("GET", "/messages(/:mailbox/:page)/asereje"),
            array("GET", "/messages(/:mailbox/:page)/asereje"),

            array("GET", "/()))(((((failback"),
            array("GET", "/test1(/:var(/var2))"),
            array("GET", "/test1(/:var(/var2))"),
            array("GET", "/test1(/:var(/var2))"),
            array("GET", "/test1(/items(/:var(/var2)))"),
            array("GET", "/test1(/items(/:var(/var2)))")
        );
        $url = array(
            array("GET", "/", true),
            array("GET", "/home", true),
            array("GET", "/home/1001", true),
            array("GET", "/home/1001/audio", true),
            array("GET", "/home/1001/audio/winter", true),

            array("GET", "/user", false),
            array("DELETE", "/user", false),
            array("GET", "/user/my-name/10001", false),
            array("GET", "/messages/inbox/10001/asereje", true),
            array("GET", "/messages/asereje", false),

            array("GET", "/failback", false),
            array("GET", "/test1/asdf", true),
            array("GET", "/test1/22/caca1", false),
            array("GET", "/test1/asdf/var2", true),
            array("GET", "/test1/items/variable/var2", true),
            array("GET", "/test1/items2/variable/var2", false)
        );

        for($i = 0 ; $i < count($patterns); $i++)
        {
            $route = new \Dunp\Http\HttpRoute($patterns[$i][0], $patterns[$i][1]);
            $scheme = \Dunp\Scheme::parseUri("http://domain.com" . $url[$i][1] );
            $_SERVER['REQUEST_METHOD'] = $url[$i][0];

            $this->assertEquals($i . " = " . $route->match($scheme), $i . " = " . $url[$i][2]);
        }
    }
}
 