<?php

//require_once __DIR__ . '/../config/config.php';
require_once __DIR__ . "/src/debug_utils.php";
require_once __DIR__ . "/src/load_utils.php";
function dunp_register_autoloader()
{
    $includes = array(__DIR__ . '/vendor/autoload.php',__DIR__ . '/src/autoloader.php');
    foreach ($includes as $value) {
        if(file_exists($value))
        {
            require_once $value;
        }
    }
    spl_autoload_register(function($class)
    {
        $file = get_file_by_class($class);
        $folders = array(__DIR__,__DIR__ . "/libs");
//        echo "loading: " . $class . "<br>"; 
        foreach ($folders as $folder) {
            $filename = $folder . "/" . $file;
            if(file_exists($filename))
            {
                include_once $filename;
                return true;
            }
        }
    });
    
    return true;
}
