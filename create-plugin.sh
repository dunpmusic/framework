#!/bin/bash
if [[ "$OSTYPE" == "linux-gnu" ]]; then
    TYPE="linux"
elif [[ "$OSTYPE" == "darwin"* ]]; then
    TYPE="mac"
elif [[ "$OSTYPE" == "cygwin" ]]; then
    TYPE="win"
elif [[ "$OSTYPE" == "msys" ]]; then
    TYPE="win"
elif [[ "$OSTYPE" == "win32" ]]; then
    TYPE="win"
elif [[ "$OSTYPE" == "freebsd"* ]]; then
    TYPE="linux"
else
    TYPE="other"
fi

if [ $# -lt 2 ]; then
	echo "Usagre: $0 <plugin-name> [page-name]"
	exit;
fi
PLUGIN_NAME=$1
PAGE_NAME=""
if [ $# -eq 2 ]; then
    PAGE_NAME=$2
fi

mkdir plugins
mkdir plugins/$PLUGIN_NAME
mkdir plugins/$PLUGIN_NAME/view
mkdir plugins/$PLUGIN_NAME/page
mkdir plugins/$PLUGIN_NAME/controller
mkdir plugins/$PLUGIN_NAME/model

VIEW="\\n\
{{#template}}normal{{/template}}\\n\
{{! Aditional styles }}\\n\
{{! Aditional scripts ([viewname].js is allways included!) }}\\n\
{{#title}}${PAGE_NAME} - Dunp{{/title}}\\n\
"
VIEW_FILE=plugins/$1/views/$2.html

PAGE="<?php\\n\
class ${PAGE_NAME}_view extends \\Dunp\\Plugin\\View\\n\
{\\n\
    public function display() {\\n\
        \\n\
    }\\n\
}\\n\
class ${PAGE_NAME}_controller extends \Dunp\Plugin\Controller\\n\
{\\n\
    public function isLoginRequired() {\\n\
        return true; // or false\\n\
    }\\n\
}"

PLUGIN="<?php\\n\
\Dunp\Plugin\PluginRegister::addPlugin(\"${PLUGIN_NAME}\");\\n\
class ${PLUGIN_NAME}_plugin extends Dunp\Plugin\Plugin\\n\
{\\n\
    /**\\n\
     * \\n\
     * @param Dunp\WebApp \$app\\n\
     * @param type $path\\n\
     */\\n\
    public function __construct(\$app, \$path) {\\n\
        parent::__construct(\$app, \$path, \"[Plugin Title]\", \"[author]\", \"[logo.png]\", \"[plugin description]\");\\n\
        \\n"
fi [ $# -eq 2 ]; then
    PLUGIN=\$this->addPage(\"$2\");\\n\
fi
        \\n\
    }\\n\
}\\n"



PAGE_FILE=plugins/$1/page/$2.php
CONTROLLER="// JS code for $2 here\n"
CONTROLLER_FILE=plugins/$1/controller/$2.js
PLUGIN_FILE=plugins/$1/plugin.php

if [ $# -eq 2 ]; then

    [[ -f $VIEW_FILE ]] || printf "$VIEW" > $VIEW_FILE

    [[ -f $PAGE_FILE ]] || printf "$PAGE" > $PAGE_FILE

    [[ -f $CONTROLLER_FILE ]] || printf "$CONTROLLER" > $CONTROLLER_FILE

fi

[[ -f $PLUGIN_FILE ]] || printf "$PLUGIN" > $PLUGIN_FILE



# Finally open this files:
if [ $TYPE == "mac" ]; then
    /Applications/NetBeans/NetBeans\ *.app/Contents/Resources/NetBeans/bin/netbeans --open $VIEW_FILE $PAGE_FILE $CONTROLLER_FILE > /dev/null
fi
