# Dunp Framework


This is the Dunp framework, this is the core of the server. 

## Instal

### Setup PHP requirements

- Debian/Ubuntu
```
sudo apt-get install libicu-dev icu-devtools php5-dev php5-intl
pecl install intl
```
- CentOS/RedHat
```
# Install dependencies
yum install gcc
yum --enablerepo=remi install php-devel zlib-devel libmemcached-devel php-pear php-pecl-memcached memcached
yum install libicu-devel
# Build componets
pecl install memcached memcache
pecl install intl
```

### Setup Framework envioriment
```
composer update
```

You must install this in the server, and the www root folder must be: public_html. This is a sample apache configuration:

```
<VirtualHost *:80>
	ServerAdmin webmaster@localhost
	DocumentRoot /var/www/html/public_html/

	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined
	<Directory /var/www/html/public_html/>
    		AllowOverride All
	</Directory>
</VirtualHost>

```

This file is located at: `/etc/apache2/sites-enabled/000-default.conf`

## Plugins

There are some plugins included by default (Namespaces) :

* system_dummy
* system_scripts
* system_installation
* system_scripts


There are some plugins to install:

* dunp_social
* dunp_cpanel


and more, you can find and manage more plugins using system_manager at `/manager`. Also
 you can install plugins manualy putting then all into `plugins` folder.


# Historical Versions

## v0.1 (current)

+ Created all scructures, and fundamentals to framework work.
+ Add Main Readme

# See more:

[plugins](/plugins/README.md)