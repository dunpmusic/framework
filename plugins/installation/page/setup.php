<?php

class setup_view extends Dunp\Plugin\View
{

    public function display() {


    }
}

class setup_controller extends \Dunp\Plugin\Route
{
    /**
     * @var \system_installation_plugin\users
     */
    var $users;
    public function __construct($app, $pagename, $basedir) {
        global $config;

        parent::__construct($app, $pagename, $basedir);

        if($config['site.configured'])
        {
            $this->setAuthorizationLevel(AUTHORIZATION_LEVEL_ADMIN);
            $this->setAuthProvider(new Dunp\Auth\IPAuthProvider());
        }
        else{
            $this->addRoute("GET", "/", array($this, "redirect" ));
        }
    }

    var $dcu_folder;
    var $config_file;

    var $dcu_error = null;
    var $config_error = null;

    var $error = false;
    var $display = true;
    var $step = array();
    var $user = null;
    var $configured = false;
    var $rand;

    var $dbname;
    var $dbuser;
    var $dbpass;
    var $dbhost;
    var $config_pass;

    var $memserver;
    var $memport;
    var $config;

    var $create_account = false;

    function redirect()
    {
        $this->getApp()->redirect("/setup");
        $this->getApp()->stop();
    }
    /**
     * @Route("GET|POST", "/setup(/:step)")
     */
    function setup($step = "1"){
        global $config;
        $this->user = posix_getpwuid(posix_geteuid());
        if(!$config['site.configured'])
        {
            if($step == "ignore")
            {
                $config['site.configured'] = true;
                $manager = $this->getApp()->getConfigManager();
                $manager->writeConfig($manager->getConfigFile(), $config);
                $this->getApp()->redirect("/");
            }
            else if($step == "1")
            {
                $this->dcu_folder = str_replace($config['upload.directory.suffix'], "", $config['upload.directory']);
                $this->config_file = $this->getApp()->getConfigManager()->getConfigFile();
                if(!isset($config['upload.directory.suffix']))
                    $this->rand = "_" . \Dunp\Functions::getRandomString();
                else
                    $this->rand = $config['upload.directory.suffix'];
                if(isset( $_POST['dcu']))
                {
                    $this->rand = $_POST['rand'];
                    $this->dcu_folder = $_POST['dcu'];
                    $finalFolder = $this->dcu_folder . "" . $this->rand;

                    //                $this->config_file = $_POST['config'];

                    if(file_put_contents($this->config_file, "", FILE_APPEND) === FALSE)
                    {
                        $this->config_error = "Not writeable";
                        $this->error = true;
                    }
//                    }
//                    else
//                    {
//                        $this->config_error = "Not exists";
//                        $this->error = true;
//                    }

                    if(!is_dir($finalFolder))
                    {
                        if(!mkdir($finalFolder))
                        {
                            $this->dcu_error = "Not found";
                            $this->error = true;
                        }
                    }
                    else
                    {
                        if(!is_writable($finalFolder) || !is_executable($finalFolder))
                        {
                            $this->dcu_error = "Not writeable";
                            $this->error = true;
                        }
                        else
                        {
                            if(file_put_contents($finalFolder . "/test.txt", get_current_user())===FALSE)
                            {
                                $this->dcu_error = "Cant not create files";
                                $this->error = true;
                            }
                        }
                    }
                    $this->display = $this->error;
                    if(!$this->error)
                    {
                        $config['upload.directory'] = $finalFolder;
                        $config['upload.directory.suffix'] = $this->rand;
                        $manager = $this->getApp()->getConfigManager();
                        if($manager->writeConfig($manager->getConfigFile(), $config) !== FALSE)
                        {
                            $this->getApp()->redirect("/setup/2");
                        }
                        else
                        {
                            $this->config_error = "Can not write to config file";
                        }
                    }
                }
                else
                {
                    $this->display = true;
                }
            }
            else if($step == "2") {
                if($this->getApp()->getConfigManager()->isConfigCreated())
                {

                    $this->config = $config;
                    $this->dbhost = $config['database.host'];
                    $this->dbuser = $config['database.user'];
    //                $this->dbpass = $config['database.pass'];
                    $this->config_pass = $config['database.pass'];
                    $this->dbname = $config['database.name'];
                    $this->memserver = $config['memcache']['host'];
                    $this->memport = $config['memcache']['port'];

                    if (isset($_POST['dbhost'], $_POST['dbuser'], $_POST['dbpass'], $_POST['dbname'])) {
                        $this->dbhost = $_POST['dbhost'];
                        $this->dbuser = $_POST['dbuser'];
                        if (empty($_POST['dbpass'])) {
                            $this->dbpass = $config['database.pass'];
                        } else {

                            $this->dbpass = $_POST['dbpass'];
                        }
                        $this->dbname = $_POST['dbname'];


                        $this->memserver = $_POST['memserver'];
                        $this->memport = $_POST['memport'];
                        if (@mysql_connect($this->dbhost, $this->dbuser, $this->dbpass)) {
                            if (@mysql_select_db($this->dbname)) {
                                $this->error = false;
                            } else {
                                $this->error = true;
                            }
                        } else {
                            $this->error = true;
                        }
                        if ($this->error) {
                            $this->error = "Can not connect to db server: " . mysql_error();
                        }
                        else{
                            $memcache = new Memcache();
                            if($memcache->connect($this->memserver, $this->memport))
                            {
                                $this->error = false;
                            }
                            else{
                                $this->error = "Can not connect to Memcache!";
                            }
                        }

                        if (!$this->error) {
                            $config['database.host'] = $this->dbhost;
                            $config['database.user'] = $this->dbuser;
                            $config['database.pass'] = $this->dbpass;
                            $config['database.name'] = $this->dbname;
                            $manager = $this->getApp()->getConfigManager();
                            $manager->writeConfig($manager->getConfigFile(), $config);

                            $this->getApp()->redirect("/setup/3");
                        }
                        $this->dbpass = isset($_POST['dbpass']) ? $_POST['dbpass'] : "";
                    }
                }
                else{
                    $this->getApp()->redirect("/setup/1");
                }
            }
            else if($step == "3")
            {
                if($this->getApp()->getDatabaseController()->getConnection()->isConnected())
                {
                    $users = new users();
                    if(!$users->hasAdminAccount())
                    {
                        $this->create_account = true;

                        if(isset($_POST['save']))
                        {
                            if(strlen($_POST['password1']) > 5)
                            {
                                if($_POST['password1'] == $_POST['password2']){
                                    $users->username = $_POST['username'];
                                    $users->email = $_POST['username'];
                                    $users->level = AUTHORIZATION_LEVEL_ADMIN;
                                    $users->hash = sha1($_POST['password1']);
                                    $users->registration_time = time();
                                    $users->name = $_POST['username'];
                                    if($users->save())
                                    {

                                        $config['site.configured'] = true;
                                        $manager = $this->getApp()->getConfigManager();
                                        $manager->writeConfig($manager->getConfigFile(), $config);
                                        $this->getApp()->redirect("/admin");
                                    }
                                    else{
                                        $this->addError("Can not save the user!");
                                    }
                                }
                                else{
                                    $this->addError("Passwords not match");
                                }
                            }
                            else
                            {
                                $this->addError("The password must be at least 6 characters");
                            }
                        }
                    }
                    else
                    {
                        if(isset($_POST['save']))
                        {
                            $config['site.configured'] = true;
                            $manager = $this->getApp()->getConfigManager();
                            $manager->writeConfig($manager->getConfigFile(), $config);
                            $this->getApp()->redirect("/admin");
                        }
                    }
                }
                else{
                    $this->getApp()->redirect("/setup/2");
                }
            }
            else
            {
                $this->getApp()->redirect("/setup/1");
            }
            $this->step[$step] = true;
        }
        else
        {
            $this->configured = true;
        }

        return true;
    }

    public function process($view)
    {
        $this->users = $this->getModel("users");
        return parent::process($view);
    }


    public function isLoginRequired() {
        return false;
    }
    public function onStart() {
        parent::onStart();
    }
    
    public function getConfigurationsTemplate() {
        return array(
        );
    }
}