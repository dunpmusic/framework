<?php
namespace system_installation_plugin;

use Dunp\Plugin\Route;
use Dunp\View\Template;
use Dunp\WebApp;

class home_view extends \Dunp\Plugin\View
{
    public function display() {
        $this->addFilter('to_json', function($data)
        {
            return json_encode($data);
        });
        $this->addFilter('to_json_beauty', function($data)
        {
            return json_encode($data, 128);
        });
    }
}

/**
 * Class Route
 * @package Dunp\Plugin
 */
class home_controller extends Route
{
    var $password;
    var $token;
    var $time;
    var $json;

    public function __construct($app, $pagename, $basedir)
    {
        parent::__construct($app, $pagename, $basedir);
        $this->time = time() + hexdec( substr( md5(print_r($app->getPluginManager()->getAllPlugins(), true)), 0, 8) );
        $this->password = sha1("");
        $this->token = md5( intval( $this->time / 60 ) );
    }

    /**
     * @Route("GET", "/public(/:id)")
     */
    public function index($password = null)
    {
        if($password)
        {
            if($password == $this->password)
            {
                $this->json = array('status' => 0, "data" => "vale, parece que eres muy listo, dime ahora que es lo siguiente", "token" => $this->token, "available" => 60 - ($this->time % 60), "week" => date("W"));
            }
        }
        else{
            $this->json = array('status' => 0, "data" => "ei! me has encontrado :)", "password" => $this->password);
        }
        $this->render("json", array("json_data" => $this->json));
    }
    public function public1()
    {

    }

    /**
     * @param $id
     * @Route("GET", "/public/:id")
     */
    public function public2($id)
    {
        die("My: " . $id);
    }


}