<?php
\Dunp\Plugin\PluginRegister::addPlugin("system_installation");

class system_installation_plugin extends Dunp\Plugin\Plugin
{
    public function __construct($app, $path) {
        parent::__construct($app, $path, "Installator Plugin", "dunpmusic", "logo.png", "Installation of the system");

        $this->addPage("setup");

        $this->addPage("home");
    }
}