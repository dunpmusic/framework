<?php

class users  extends \Dunp\Model\DesignerModel
{
    ///     ("var type", Null?, Default value, Autoincrement)
    /**
     * @Field("int(11)",False,Null,True)
     * @Index("PRIMARY")
     */
    var $id;
    /**
     * @Field("varchar(64)",True,Null,False)
     * @Index("email")
     */
    var $email;
    /**
     * @Field("varchar(32)",True,Null,False)
     */
    var $username;
    /**
     * @Field("varchar(32)",True,Null,False)
     */
    var $hash;
    /**
     * @Field("varchar(16)",True, "md5", False)
     */
    var $hash_algorithm;
    /**
     * @Field("varchar(128)",True,Null,False)
     */
    var $token;
    /**
     * @Field("varchar(16)",True,Null,False)
     */
    var $name;
    /**
     * @Field("varchar(255)",False,"/img/avatar.png",False)
     */
    var $avatar;
    /**
     * @Field("int(11)",True,Null,False)
     */
    var $registration_time;
    /**
     * @Field("varchar(32)",True,Null,False)
     */
    var $ip;
    /**
     * @Field("int(11)",True,Null,False)
     */
    var $level;
    /**
     * @Field("varchar(10)",True,Null,False)
     */
    var $gender;
    /**
     * @Field("int(11)",True,Null,False)
     */
    var $referred;

    function __construct($data = null)
    {
        parent::__construct("users");
        if($data)
        {
            \Dunp\Functions::setArrayData($this, $data);
        }
    }


    public function install()
    {
        // TODO: Implement install() method.
    }

    /**
     * @param $index
     * @return users
     */
    public function get($index)
    {
        // TODO: Implement install() method.

        $index = $this->getDb()->escape($index);
        return $this->parse($this->getDb()->query("SELECT * FROM users WHERE id = $index"));
    }

    /**
     * @param $email
     * @param $password
     * @return users
     */
    public function getByEmailPassword($email, $password)
    {
        $email = $this->getDb()->escape($email);
//        $password = $this->getDb()->escape($this->getHashPassword($password), DATABASE_CONNECTION_FORCE_TYPE_STRING);
        $user = $this->parse($this->getDb()->query("SELECT * FROM users WHERE email = $email"));
        if($user->hash == $this->getHashPassword($password, $user->hash_algorithm))
        {
            return $user;
        }
        return null;
    }

    public function save()
    {
        // TODO: Implement save() method.

        if($this->exists())
        {
            // update
            $index = $this->getDb()->escape($this->id);
            return $this->getDb()->exec("UPDATE users SET id = ". $this->getDb()->escape($this->id) . ", email = ". $this->getDb()->escape($this->email) . ", username = ". $this->getDb()->escape($this->username) . ", hash = ". $this->getDb()->escape($this->hash) . ", token = ". $this->getDb()->escape($this->token) . ", name = ". $this->getDb()->escape($this->name) . ", avatar = ". $this->getDb()->escape($this->avatar) . ", registration_time = ". $this->getDb()->escape($this->registration_time) . ", ip = ". $this->getDb()->escape($this->ip) . ", level = ". $this->getDb()->escape($this->level) . ", gender = ". $this->getDb()->escape($this->gender) . ", referred = ". $this->getDb()->escape($this->referred) . " WHERE id = $index");
        }
        else
        {
            if($this->getDb()->exec("INSERT INTO users (id, email, username, hash, token, name, avatar, registration_time, ip, level, gender, referred, hash_algorithm) VALUES (". $this->getDb()->escape($this->id) . ", ". $this->getDb()->escape($this->email) . ", ". $this->getDb()->escape($this->username) . ", ". $this->getDb()->escape($this->hash) . ", ". $this->getDb()->escape($this->token) . ", ". $this->getDb()->escape($this->name) . ", ". $this->getDb()->escape($this->avatar) . ", ". $this->getDb()->escape($this->registration_time) . ", ". $this->getDb()->escape($this->ip) . ", ". $this->getDb()->escape($this->level) . ", ". $this->getDb()->escape($this->gender) . ", ". $this->getDb()->escape($this->referred) . ", ". $this->getDb()->escape($this->hash_algorithm) . ")"))
            {
                $this->id = $this->getDb()->getLastInsertId();
                return true;
            }
        }

    }

    public function exists()
    {
        // TODO: Implement exists() method.

        if(!empty($this->id))
        {

            $index = $this->getDb()->escape($this->id);
            return count($this->getDb()->query("SELECT * FROM users WHERE id = $index")) > 0;
        }
        return false;

    }

    public function delete()
    {
        // TODO: Implement delete() method.

    }

    public function isAdmin()
    {
        return $this->level >= AUTHORIZATION_LEVEL_ADMIN;
    }

    public function hasAdminAccount()
    {
        return count($this->getDb()->query("SELECT * FROM users WHERE level >= " . AUTHORIZATION_LEVEL_ADMIN)) > 0;
    }
    public function parse($data, $array = false)
    {

        if(count($data) == 1)
        {
            if($array)
            {
                return array(new users($data[0]));
            }
            else
            {
                return new users($data[0]);
            }
        }
        else if(count($data) > 1)
        {
            $items = array();
            foreach($data as $item)
            {
                $items[] = new users($item);
            }
            return $items;
        }
        return false;
    }

    public function getHashPassword($plain, $algorithm = "md5")
    {
        if($algorithm == "md5")
            return md5($plain);
        return
            hash($algorithm, $plain);
    }

    public function getToken()
    {
        if(empty($this->token) && !empty($this->id))
        {
            $this->token = \Dunp\Functions::getRandomString($this->getField("token")->getLength());
            $this->save();
        }
        return $this->token;
    }
}