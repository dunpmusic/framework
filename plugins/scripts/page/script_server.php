<?php
class script_server_view extends \Dunp\Plugin\View
{
    public function display() {
        global $config;
        
        $items = Dunp\Request::getParameters();
        $hash = \Dunp\Request::getParameter(count($items) - 3);
        $file = \Dunp\Request::getParameter(count($items) - 2);
        $ext = substr($file, strrpos($file, ".") + 1);
        if($ext == "css")
        {
            header("Content-Type: text/css");
            if($hash == "flush")
            {
                $files = $this->globRecursive($config['upload.directory'], "_css_*");
                $items = 0;
                foreach ($files as $value) {
                    unlink($value);
                    $items++;
                }
                die("Unlink $items items");
            }
            else
            {
                $cache = $config['upload.directory']."/_css_" . $hash . ".css";
                if(file_exists($cache))
                {
                    
                    readfile($cache);
                }
                else
                {
                    header('HTTP/1.1 404 Not Found');
                    echo "<h2>Cache not found</h2>";
                }
            }
        }
        else
        {
            
            header("Content-Type: text/javascript");
            if($hash == "flush")
            {
                $files = $this->globRecursive($config['upload.directory'], "_scripts_*");
                $items = 0;
                foreach ($files as $value) {
                    unlink($value);
                    $items++;
                }
                die("Unlink $items items");
            }
            else
            {
                $cache = $config['upload.directory']."/_scripts_" . $hash . ".js";
                if(file_exists($cache))
                {
                    
                    readfile($cache);
                }
                else
                {
                    header('HTTP/1.1 404 Not Found');
                    echo "<h2>Cache not found</h2>";
                }
            }
        }
        return false;
    }
    
    function globRecursive($path, $find) {
        $list = array();
        $dh = opendir($path);
        while (($file = readdir($dh)) !== false) {
            if (substr($file, 0, 1) == '.') continue;
            $rfile = "{$path}/{$file}";
            if (is_dir($rfile)) {
                foreach ($this->globRecursive($rfile, $find) as $ret) {
                    $list[] = $ret;
                }
            } else {
                if (fnmatch($find, $file)) $list[] = $rfile;
            }
        }
        closedir($dh);
        return $list;
    }

}

class script_server_controller extends \Dunp\Plugin\Controller
{
    public function onHandleUrl($params) {
        if(count($params) >= 3)
        {
            if($params[count($params) - 3] == "machine")
            {
                return TRUE;
            }
        }
    }
}