<?php
\Dunp\Plugin\PluginRegister::addPlugin("system_scripts");

class system_scripts_plugin extends Dunp\Plugin\Plugin
{
    /**
     * 
     * @param Dunp\WebApp $app
     * @param type $path
     */
    public function __construct($app, $path) {
        parent::__construct($app, $path, "Script Server", "dunpmusic", "logo.png", "Plugin para capturar los scripts minificados");
        
        $this->addPage("script_server");
        
    }
}

