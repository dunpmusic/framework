<?php
\Dunp\Plugin\PluginRegister::addPlugin("system_cron");
class system_cron_plugin extends Dunp\Plugin\Plugin
{
    /**
     *
     * @param Dunp\WebApp $app
     * @param type
     */
    public function __construct($app, $path) {
        parent::__construct($app, $path, "Cron Task Manager", "dunpmusic");

        $this->addCliCommand("cron");
//        $this->addCronJob("task1", array("manus1", "arg3"));
    }

    public function onRegisterManagers($app)
    {
    }
}
