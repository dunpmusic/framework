<?php
/**
 * Created by PhpStorm.
 * User: manus
 * Date: 9/6/15
 * Time: 22:52
 */

class cron_cli extends \Dunp\Plugin\Cli
{
    function run($argc, $argv)
    {
        if($argv[1] == "list")
        {
            $manager = $this->getApp()->getManager(CRON_MANAGER);
            if($manager instanceof CronManager)
            {
                echo "Running jobs:\n";
                $manager->listJobs();
            }
            die();
        }
        
        if($argc == 2)
        {
            $text = $argv[1];
            $namespace = substr($text,0, strrpos($text, "::"));
            $task = substr($text,strrpos($text, "::") + 2);
            $plugins = $this->getApp()->getPluginManager()->getPlugins();
            foreach($plugins as $plugin)
            {
                if($plugin->getNamespace() == $namespace)
                {
                    $jobs = $plugin->getCronJobs();
                    foreach($jobs as $job)
                    {
                        if($job->getTask() == $task)
                        {
                            $classname = $task . "_cron";
                            $filename = $plugin->getPath() . "/cron/$task.php";
                            include_once $filename;
                            if(class_exists($classname))
                            {
                                $class = new $classname($this->getApp());
                                if($class instanceof \Dunp\Cron\Cron)
                                {
                                    return call_user_func_array(array($class, "exec"), $job->getArgs());
                                }
                            }
                        }
                    }
                    return false;
                }
            }
            return false;

        }
        else{
            echo "Usagre: cron [task_id]\n";
        }

        return true;
    }
}