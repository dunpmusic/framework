<?php

class cron_tasks  extends \Dunp\Model\DesignerModel
{
    /**
     * @Field("int(11)",False,Null,True)
     * @Index("PRIMARY")
     */
    var $id;
    /**
     * @Field("varchar(64)",False,Null,False)
     */
    var $function;
    /**
     * @Field("varchar(2000)",False,Null,False)
     */
    var $args;
    /**
     * @Field("varchar(256)",False,Null,False)
     */
    var $cron;
    /**
     * @Field("varchar(256)",False,Null,False)
     */
    var $namespace;

    function __construct($data = null)
    {
        parent::__construct("cron_tasks");
        if($data)
        {
            $this->id = $data['id'];
            $this->function = $data['function'];
            $this->args = $data['args'];
            $this->cron = $data['cron'];
            $this->namespace = $data['namespace'];

        }
    }


    public function install()
    {
        // TODO: Implement install() method.
    }

    /**
     * @param $index
     * @return cron_tasks
     */
    public function get($index)
    {
        // TODO: Implement install() method.

        $index = $this->getDb()->escape($index);
        return $this->parse($this->getDb()->query("SELECT * FROM cron_tasks WHERE id = '$index'"));
    }

    public function save()
    {
        // TODO: Implement save() method.

        if($this->exists())
        {
            // update
            $index = $this->getDb()->escape($this->id);
            return $this->getDb()->exec("UPDATE cron_tasks SET id = '". $this->getDb()->escape($this->id) . "', function = '". $this->getDb()->escape($this->function) . "', args = '". $this->getDb()->escape($this->args) . "', cron = '". $this->getDb()->escape($this->cron) . "' WHERE id = '$index'");
        }
        else
        {
            return $this->getDb()->exec("INSERT INTO cron_tasks (id, function, args, cron) VALUES ('". $this->getDb()->escape($this->id) . "', '". $this->getDb()->escape($this->function) . "', '". $this->getDb()->escape($this->args) . "', '". $this->getDb()->escape($this->cron) . "')");
        }

    }

    public function exists()
    {
        // TODO: Implement exists() method.

        $index = $this->getDb()->escape($this->id);
        return count($this->getDb()->query("SELECT * FROM cron_tasks WHERE id = '$index'")) > 0;

    }

    public function delete()
    {
        // TODO: Implement delete() method.
        $id = $this->getDb()->escape($this->id);
        return $this->getDb()->exec("DELETE FROM {$this->tableName} WHERE id = '$id'" );

    }

    public function selectByFunction($function)
    {
        $function = $this->getDb()->escape($function);
        return $this->parse($this->getDb()->query("SELECT * FROM {$this->tableName} WHERE function = '$function'"));
    }
    public function parse($data, $array = false)
    {
        if(count($data) == 1)
        {
            if($array)
            {
                return array(new cron_tasks($data[0]));
            }
            else
            {
                return new cron_tasks($data[0]);
            }
        }
        else if(count($data) > 1)
        {
            $items = array();
            foreach($data as $item)
            {
                $items[] = new cron_tasks($item);
            }
            return $items;
        }
        return false;
    }

    public function getCommand()
    {
        $path = realpath(__DIR__ . "/../../../dunp");
        return $this->cron . " 'php $path cron {$this->id}'";
    }
}