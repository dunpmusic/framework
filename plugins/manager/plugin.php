<?php
\Dunp\Plugin\PluginRegister::addPlugin("system_manager");
class system_manager_plugin extends Dunp\Plugin\Plugin
{
    /**
     * 
     * @param Dunp\WebApp $app
     * @param type 
     */
    public function __construct($app, $path) {
        parent::__construct($app, $path, "System Manager", "dunpmusic", "[logo.png]", "[plugin description]");

        $this->addDependence("dunpmusic/mysql-auth");
        $this->addCliCommand("manager");
        $this->addCliCommand("config");
        $this->addCliCommand("database");
        $this->addCliCommand("create");

        $this->addPage("admin");
        $this->addPage("admin/plugins");
    }
}
