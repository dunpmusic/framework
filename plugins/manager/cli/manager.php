<?php

class manager_cli extends \Dunp\Plugin\Cli {

    public function run($argc, $argv) {
        if ($argc > 1) {
            if ($argv[1] == "setup") {
                $this->setup();
                $this->build();
            } else if ($argv[1] == "build") {
                $this->build();
            } else if ($argv[1] == "plugin") {
                if($argc > 2)
                {
                    if($argv[2] == "add")
                    {
                        if($argc > 3)
                            $this->addPlugin($argv[3]);
                        else
                            $this->usagre ($argv);
                    }
                    else if($argv[2] == "remove")
                    {
                        if($argc > 3)
                            $this->removePlugin($argv[3]);
                        else
                            $this->usagre ($argv);
                    }
                    else if($argv[2] == "update")
                    {
                        if($argc > 3)
                            $this->updatePlugin($argv[3]);
                        else
                            $this->usagre ($argv);
                    }
                    else if($argv[2] == "disable")
                    {
                        if($argc > 3)
                            $this->setPluginEnabled($argv[3], false);
                        else
                            $this->usagre ($argv);
                    }
                    else if($argv[2] == "enable")
                    {
                        if($argc > 3)
                            $this->setPluginEnabled($argv[3], true);
                        else
                            $this->usagre ($argv);
                    }
                    else
                    {
                        $plugins = $this->getApp()->getPluginManager()->getPlugins();

                        echo "== Plugins enabled ==\n";
                        foreach ($plugins as $value) {
                            
                            echo "* " .$value->getNamespace() . " => " . Helper::normalizePath( $value->getPath() ) . "\n";
                        }
                        echo "== Plugins disabled ==\n";
                        $p = $this->getApp()->getPluginManager()->getPluginsDisabled();
                        foreach ($p as $value) {

                            echo "* " . $value. "\n";
                        }
                    }
                }
                else
                {
                    $this->pluginsRefresh();
                }
            } else {
                $this->usagre($argv);
            }
        } else {
            $this->usagre($argv);
        }
        return true;
    }

    function commands() {
        $array = array("composer", "npm");
        foreach ($array as $command) {

            $cmd = "command -v $command >/dev/null && continue || { echo '$command command not found.'; exit 1; }";
            $ret = 0;
            $int = 0;
            exec($cmd, $ret, $int);

            if ($int != 0) {
                echo "Error: Require command not found\n";
                echo "Command not found: '$command'\n";
                
                $this->commandSetupHelp($command);
//                return false;
            }
        }
        return true;
    }
    
    function getMachineInfo()
    {
        $info = array();
        if(PHP_OS == "Linux")
        {
            $info[] = "linux";
            $info[] = "unix";
        }
        if(PHP_OS == "DarwinMac")
        {
            $info[] = "mac";
            $info[] = "unix";
        }
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $info[] = "win";
        }
        
        $version = php_uname("v");
        
        if(strpos($version, "Ubuntu") !== 0)
        {
            $info[] = "ubuntu";
        }
        if(strpos($version, "SMP") !== 0)
        {
            $info[] = "centos";
        }
        
        return $info;
    }

    function commandSetupHelp($command) {
        $commands = array(
            "composer" => array(
                "linux" => "curl -sS https://getcomposer.org/installer | php -- --install-dir=/bin --filename=composer"
            ),
            "npm" => array(
                "ubuntu" => "apt-get install npm",
                "mac" => "brew install npm",
                "centos" => "yum install npm",
            )
        );
        if(isset($commands[$command]))
        {
            $actions = $commands[$command];
            $info = $this->getMachineInfo();
            foreach ($actions as $key => $value) {
                if(in_array($key, $info))
                {
                    echo $value . "\n";
                    break;
                }
            }
        }
        else
        {
            echo "Can't not find any help to install the command: '$command'\n";
        }
        
        
    }
    
    private function exec($command)
    {
        $res = "";
        $val = 0;
        passthru($command, $val);
        if($val != 0)
        {
            echo "Error executing command: '$command'\n";
            return false;
        }
        
        return true;
    }

    private function updateComposer($root) {
        if (file_exists($root . "/composer.json")) {
            return $this->exec("cd '$root' && composer update");
        }
    }

    private function updateNpm($root) {
        if (file_exists($root . "/package.json")) {
            return $this->exec("cd '$root' && npm install");
        }
    }

    function setup() {
        if ($this->commands()) {
            $root = realpath(__DIR__ . "/../../../");
            $this->updateComposer($root);
            $this->updateNpm($root);
            $plugins = $this->getApp()->getPluginManager()->getPlugins();

            foreach ($plugins as $plugin) {
                $root = $plugin->getPath();
                echo "Setup plugin: " . $plugin->getNamespace() . " ...\n";
                $this->updateComposer($root);
                $this->updateNpm($root);
                
                foreach ($plugin->getSetupSteps() as $setup) {
                    $this->exec("cd '$root' && $setup");
                }
            }
        }
    }

    public function updatePlugin($namespace)
    {
        return $this->download($namespace);
    }
    public function addPlugin($namespace, $version = "*")
    {
        $this->setPluginEnabled($namespace, true);
        
        $this->pluginsRefresh();
    }
    
    public function removePlugin($namespace)
    {
        
        $req = $this->pluginsCheckDependencies(array($namespace));
        
        if(!in_array($namespace, $req))
        {

            $this->setPluginEnabled($namespace, false);
        }
        else
        {
            echo "Can't remove the plugin: $namespace, because is required by other\n";
        }
    }
    
    public function pluginsGetDefined()
    {
        global $config;
        $toInstall = array();
        $plugins = $this->getApp()->getPluginManager()->getPlugins();
        $namespaces = array_map(function($plugin){ return $plugin->getNamespace() ;}, $plugins);
        foreach ($config['plugins'] as $key => $value) {
            if(!in_array($key, $namespaces))
            {
                if(!in_array($key, $toInstall))
                {
                    $toInstall[$key][] = "";
                }
            }
        }
        
        return $toInstall;
    }
    public function pluginsCheckDependencies($ignore = array())
    {
        global $config;
        
        $plugins = $this->getApp()->getPluginManager()->getPlugins();
        $namespaces = array_map(function($plugin){ return $plugin->getNamespace() ;}, $plugins);
        foreach ($ignore as $value) {
            if(in_array($value, $namespaces))
            {
                unset($namespaces[array_search($value, $namespaces)]);
            }
        }
        $info = array();
        foreach ($plugins as $plugin) {
            $dependences = $plugin->getDependences();
            foreach ($dependences as $dependence) {
                if(!in_array($dependence, $namespaces))
                {
                    $info[$dependence][] = $plugin->getNamespace();
                }
            }
        }
        
        return ($info);
    }
    
    public function pluginsRefresh(& $done = array())
    {
        echo "Verify plugins dependencies...\n";
        $toInstall = $this->pluginsCheckDependencies();
        $toInstall = array_merge($toInstall,$this->pluginsGetDefined());

        foreach ($toInstall as $key => $value) {

            if(count($value) >= 1 && $value[0] != "")
            {
                echo "=> Installing '$key', required by: " . implode(", ", array_map(function($text){return "'$text'";}, $value)) . "\n";
            }
        }
        foreach ($toInstall as $key => $value) {
            if(!$this->installPlugin($key, $done))
            {
                if(count($value) >= 1 && $value[0] != "")
                {
                    echo "[ERROR] Please verify the dependencies of the project(s): " . implode(", ", array_map(function($text){return "'$text'";}, $value)) . "\n";
                }
                else
                {
                    echo "[ERROR] Please ensure the plugin name is correct in configuration. Current name: '$key'\n";
                }
            }
        }
        return count($toInstall);

    }
    
    public function installPlugin($namespace, & $done = array())
    {
        if(!in_array($namespace, $done))
        {
            $done[] = $namespace;
            if($this->download($namespace))
            {
                $this->setPluginEnabled($namespace, true);
                $this->setup();
                $this->pluginsRefresh($done);
                return true;
            }
        }
        else{
            echo "[ERROR] Plugin '$namespace' could not install!\n";
        }
        return false;
    }

    function build() {

        $plugins = $this->getApp()->getPluginManager()->getPlugins();

        foreach ($plugins as $plugin) {
            $root = $plugin->getPath();
            echo "Build plugin: " . $plugin->getNamespace() . " ...\n";
            
            foreach ($plugin->getBuildSteps() as $build) {
                $this->exec("cd '$root' && $build");
            }
        }
    }

    function usagre($argv) {
        printf("Usagre: %s <command> [parameters]\n", $argv[0]);
        printf("Commands:\n");
        printf("setup: \n");
        printf("\tSetup and build the framework envioriment \n");
        printf("build:\n");
        printf("\tBuild plugins that need \n");
        printf("plugin: \n");
        printf("\tPlugin manager.\n");
        printf("\t\tadd: \n");
        printf("\t\t\tAdd new plugin.\n");
        printf("\t\t\targs:\n");
        printf("\t\t\t\t[name]: plugin name.\n");
        printf("\t\tupdate: \n");
        printf("\t\t\tUpdate the plugin.\n");
        printf("\t\t\targs:\n");
        printf("\t\t\t\t[name]: plugin name.\n");
        printf("\t\tremove: \n");
        printf("\t\t\tRemove the plugin.\n");
        printf("\t\t\targs:\n");
        printf("\t\t\t\t[name]: plugin name.\n");
        printf("\t\tdisable: \n");
        printf("\t\t\tDisable the plugin.\n");
        printf("\t\t\targs:\n");
        printf("\t\t\t\t[name]: plugin name.\n");
        printf("\t\tenable: \n");
        printf("\t\t\tEnable the plugin.\n");
        printf("\t\t\targs:\n");
        printf("\t\t\t\t[name]: plugin name.\n");
        printf("\t\tlist: \n");
        printf("\t\t\tList all plugins.\n");
    }
    
    public function getName($namespace)
    {
        if(strrpos($namespace, "/") !== FALSE)
        {
            return substr($namespace, strrpos($namespace, "/") + 1);
        }
        return $namespace;
    }

    public function setPluginEnabled($namespace, $state)
    {

        global $config;
        if($state)
        {
            $config['plugins'][$namespace] = "*";

        }
        else{
            unset($config['plugins'][$namespace]);
        }
        $this->getApp()->getConfigManager()->writeDefaultConfig($config);
        echo "** Plugin $namespace is now: " . ($state?"ENABLED" : "DISABLED") . "\n";
    }
    
    public function download($namespace)
    {
        $folder = $this->getName($namespace);
        $data = $this->getApp()->getDefinitionsManager()->get(array("repository" => $namespace));
        foreach ($data->providers as $provider) {
            if($provider->type == "git")
            {
                $git = $provider->git;
                echo "Trying: {$git}...\n";
                $target = $this->getApp()->getPluginManager()->getPluginsFolder() . "/$folder";
                if(!$this->git($git, $target))
                {
                    $git = $provider->https;
                    echo "Trying: {$git}...\n";
                    return $this->git($git, $target);
                }
                return true;
            }
        }
        
        return false;
    }

    public function git($git, $target)
    {
        if(!is_dir($target))
        {

            if($this->exec("git clone $git $target"))
            {
                return true;
            }
        }
        else{
            if($this->exec("cd '$target' && git pull"))
            {
                return true;
            }
        }
        return false;
    }

}
