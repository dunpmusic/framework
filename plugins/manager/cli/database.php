<?php

class database_cli extends \Dunp\Plugin\Cli
{

    function run($argc, $argv)
    {
        // TODO: Implement run() method.
        $export = new \Dunp\Database\SQLGenerator();
        $tables = $export->getDatabaseStructure();

        if(isset($argv[1]))
        {
            $table = $tables[$argv[1]];
            $php = "";
            $construct = "";
            $exists = "";
            $save = "";
            $get = "";
            $delete = "";
            foreach($table->fields as $field)
            {
                $construct .= "            \$this->$field->name = \$data['$field->name'];\n" ;
                $php .= "/**\n";
                $php .= "* @Field(\"{$field->type}\",". ($field->null?"True":"False") . "," . ($field->default!=""?"\"" . $field->default . "\"":"Null") . ",". ($field->autoincrement?"True":"False") . ")\n";
                foreach($table->indexes as $index)
                {
                    if($index->field == $field->name)
                    {
                        $php .= "* @Index(\"{$index->type}\")\n";
                    }
                }
                foreach($table->constraints as $constraint)
                {
                    if($constraint->field == $field->name)
                    {
                        $php .= "* @Constraint(\"{$constraint->foreignTable}\", \"{$constraint->foreignField}\")\n";
                    }
                }

                $php .= "*/\n";
                $php .= "var \$$field->name;\n";

            }

            foreach($table->constraints as $constraint)
            {

                $php .= "/**\n";
                $php .= "* @var $constraint->foreignTable\n";
                $php .= "*/\n";
                $php .= "var \$$constraint->foreignTable;\n";

                $construct .= "            \$this->$constraint->foreignTable = new $constraint->foreignTable();\n" ;
                $construct .= "            \$this->$constraint->foreignTable = \$this->$constraint->foreignTable->get(\$this->$constraint->field);\n" ;
            }
            $index = false;
            foreach($table->indexes as $i)
            {
                if($i->type == "PRIMARY")
                {
                    $index = $i->field;
                    break;
                }
            }
            if($index)
            {
                $get = '
$index = $this->getDb()->escape($index);
return $this->parse($this->getDb()->query("SELECT * FROM '.$table->name.' WHERE '.$index.' = $index"));';

                $exists = '
$index = $this->getDb()->escape($this->'. $index.');
return count($this->getDb()->query("SELECT * FROM '.$table->name.' WHERE '.$index.' = $index")) > 0;
';

                $save_update = "";
                $save_insert = "";
                $save_insert1 = "";
                $save_insert2 = "";
                foreach($table->fields as $key => $f)
                {
                    $save_update .= ($save_update=="")?"":", ";
                    $save_insert1 .= ($save_insert1=="")?"":", ";
                    $save_insert2 .= ($save_insert2=="")?"":", ";
                    $save_update .= "$key = \". \$this->getDb()->escape(\$this->$key) . \"";
                    $save_insert1 .= $key;
                    $save_insert2 .= "\". \$this->getDb()->escape(\$this->$key) . \"";
                }
                $save_update = "UPDATE $table->name SET $save_update WHERE $index = \$index";
                $save_insert = "INSERT INTO $table->name ($save_insert1) VALUES ($save_insert2)";
                $save = '
if($this->exists())
{
    // update
    $index = $this->getDb()->escape($this->'. $index.');
    return $this->getDb()->exec("'.$save_update.'");
}
else
{
    return $this->getDb()->exec("'.$save_insert.'");
}
';
            }
            else{
                $get = $save = $delete = $exists = "//Can not generate this field, because PRIMARY index not found in the table";
            }
            $php = $this->indent($php, 4);
            $get = $this->indent($get, 8);
            $save = $this->indent($save, 8);
            $delete = $this->indent($delete, 8);
            $exists = $this->indent($exists, 8);
            echo '
class ' . $table->name . '  extends \Dunp\Model\DesignerModel
{
    '. $php .'
    function __construct($data = null)
    {
        parent::__construct("' . $table->name . '");
        if($data)
        {
'.$construct.'
        }
    }


    public function install()
    {
        // TODO: Implement install() method.
    }

    public function get($index)
    {
        // TODO: Implement install() method.
'. $get . '
    }

    public function save()
    {
        // TODO: Implement save() method.
'. $save . '
    }

    public function exists()
    {
        // TODO: Implement exists() method.
'. $exists . '
    }

    public function delete()
    {
        // TODO: Implement delete() method.
'. $delete . '
    }
    public function parse($data, $array = false)
    {
        if(count($data) == 1)
        {
            if($array)
            {
                return array(new '. $table->name. '($data[0]));
            }
            else
            {
                return new '. $table->name. '($data[0]);
            }
        }
        else if(count($data) > 1)
        {
            $items = array();
            foreach($data as $item)
            {
                $items[] = new '. $table->name. '($item);
            }
            return $items;
        }
        return false;
    }
}
';
        }
        else{

            echo "Available tables:\n";
            foreach($tables as $key => $table)
            {
                echo $key . "\n";
            }
        }
        return true;

//        $this->getApp()->getDatabaseController()->getTable();
    }

    private function indent($text, $size){
        $data = str_repeat(" ", $size);

        return $data . implode("\n$data", explode("\n", $text));
    }
}