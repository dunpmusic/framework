<?php
/**
 * Created by PhpStorm.
 * User: manus
 * Date: 14/6/15
 * Time: 21:06
 */

class create_cli extends \Dunp\Plugin\Cli
{

    function run($argc, $argv)
    {
        // TODO: Implement run() method.

        if($argc > 2)
        {

            $path = $argv[1];
            $name = $argv[2];
            $vendor = "vendor";
            if($argc > 3)
            {
                $vendor = $argv[3];
            }

            mkdir($path);
            $plugin = "$path/plugin.php";

            $template = '<?php
\Dunp\Plugin\PluginRegister::addPlugin("' . $name . '");
class ' . $name . '_plugin extends Dunp\Plugin\Plugin
{
    /**
     *
     * @param Dunp\WebApp $app
     * @param type
     */
    public function __construct($app, $path) {
        parent::__construct($app, $path, "' . $name . '", "' . $vendor . '");
        // Add cmd line command
        //$this->addCliCommand("command_line_example");

        // Add page (/example)
        // $this->addPage("example");


    }
}';
            file_put_contents($plugin, $template);

            mkdir($path . "/page");
            mkdir($path . "/cli");
            mkdir($path . "/view");
            mkdir($path . "/src");
            mkdir($path . "/websocket");
            $page = "$path/page/example.php";
            $template = '<?php
// Optional: Des-Comment this line if there are page names conflicts
// namespace '. $name .'_plugin;

class example_view extends \Dunp\Plugin\View
{

    public function display()
    {
        // TODO: Implement display() method.
    }
}
class example_controller extends \Dunp\Plugin\Route
{
    public function __construct($app, $pagename, $basedir)
    {
        parent::__construct($app, $pagename, $basedir); // TODO: Change the autogenerated stub
    }


    public function isLoginRequired()
    {
        return false;
    }

    /**
     * @Route("GET|POST", "/example1(/:optionalValue)")
     */
    public function exampleRoute($optionalValue = "")
    {
        $this->render("example");
        return true;
    }

}
';
            file_put_contents($page, $template);
            $view = "$path/view/example.html";

            $template = '
{{#theme}}bootstrap{{/theme}}
{{#template}}normal{{/template}}
{{#title}}Hello!{{/title}}

<h3>Hello World!</h3>';
            file_put_contents($view, $template);

            \Dunp\Log::v("Plugin created in: $path, name = $name, vendor = $vendor");

        }
        else{
           \Dunp\Log::v(sprintf("Usagre: %s <path> <plugin_name> [vendor]", $argv[0]));
        }

        return true;
    }
}