<?php

class config_cli extends \Dunp\Plugin\Cli
{
    public function run($argc, $argv) {
        
        global $config;
        if($argc == 1)
        {
            $this->listConfig();
            
        }
        else if($argc == 2)
        {
            $this->set($argv[1]);
        }
        else if($argc > 2)
        {
            $output = array();
            $action = ($argv[1]);
            if($action == "append")
            {
                
                $this->buildRecursive(array_slice($argv, 2), 0, $output);
                $config = array_merge_recursive($config, $output);
            }
            else if($action == "remove")
            {
                $this->unsetRecursive(array_slice($argv, 2), $config);
            }
            else
            {
                $this->buildRecursive(array_slice($argv, 1),0, $output);
                $config = array_merge($config, $output);
            }
            $this->getApp()->getConfigManager()->writeDefaultConfig($config);
            echo "New config: \n";
            print_r($config);
        }
        
        return true;
    }
    
    public function buildRecursive($array, $index = 0, &$output = array())
    {
        if(count($array) - 2 == $index)
        {
            $output[$array[$index]] = $array[$index + 1];
        }
        else if(count($array) - 1 > $index)
        {
            
            $this->buildRecursive($array, $index + 1, $next);
            $output[$array[$index]] = $next;
        }
    }
    public function unsetRecursive($array, & $target, $index = 0)
    {
        
        if(count($array) - 1 >= $index)
        {
            if(isset($target[$array[$index]]))
            {
                if(count($array) - 1 == $index)
                {
                    unset($target[$array[$index]]);
                }
                else
                {
                    $this->unsetRecursive($array, $target[$array[$index]], $index + 1);
                }
            }
        }
    }
    
    public function set($value)
    {
        global $config;
        
        
        if($json = json_decode($value, true))
        {
            $config = array_merge($config, $json);
            $this->getApp()->getConfigManager()->writeDefaultConfig($config);
        }
        else
        {
            echo "JSON bad format\n";
        }
        
    }
    
    public function listConfig()
    {
        global $config;
        print_r($config);
    }

}