<?php
\Dunp\Plugin\PluginRegister::addPlugin("system_dummy");

class system_dummy_plugin extends Dunp\Plugin\Plugin
{
    public function __construct($app, $path) {
        parent::__construct($app, $path, "Dummy Plugin", "dunpmusic", "logo.png", "Es plugin no hace absolutamente nada");
        $this->addCliCommand("dummy");
        $this->addPage("dummy");
        $this->addPage("folder/dummy2");

        $this->addWebSocket("dummy");
    }
}