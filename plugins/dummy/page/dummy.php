<?php

class dummy_view extends Dunp\Plugin\View
{
    public function display() {
    }
}

class dummy_controller extends \Dunp\Plugin\Controller
{
    public function onRegisterAPI(\Slim\Slim $api, $callback) {
        parent::onRegisterAPI($api, $callback);
        
        $api->get('/dummy', function() use($api, $callback)
        {
//            
            $clients = $this->getTable("clients");
            $list = ($clients->getFields());
            
            $callback( array("status" => true, "mydata_return" => "manuel", "list" => $list) );
        });
    }
    
    public function getConfigurationsTemplate() {
        return array("dummy.enabled" => false);
    }
}