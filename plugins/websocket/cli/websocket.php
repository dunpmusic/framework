<?php

class websocket_cli extends \Dunp\Plugin\Cli
{

    function run($argc, $argv)
    {
        // TODO: Implement run() method.
        $pidFile = \Dunp\Functions::getFilePath("websocket.pid");
        if($argc == 2)
        {
            if($argv[1] == "-r")
            {
                $this->getApp()->getManager(WEBSOCKET_MANAGER)->restart();

                return true;

            }
        }

        if(!\Dunp\Functions::isProcessRunning($pidFile, true))
        {
            file_put_contents($pidFile, getmypid());

            \Dunp\Log::d("Websocket server is running!");
            \Dunp\Log::d("Starting at: " . date("d-m-Y H:i:s", time()));
            $config = $this->getApp()->getConfigManager()->get();
            $uri = "{$config->websocket['host']}:{$config->websocket['port']}";
            $loop = \React\EventLoop\Factory::create();
            $logger = new \Zend\Log\Logger();
            $writer = new Zend\Log\Writer\Stream("php://output");
            $logger->addWriter($writer);
            $server = new \Devristo\Phpws\Server\WebSocketServer("tcp://$uri", $loop, $logger);

            $router = new \Devristo\Phpws\Server\UriHandler\ClientRouter($server, $logger);

            $plugins = $this->getApp()->getPluginManager()->getPlugins();
            foreach($plugins as $plugin)
            {
                foreach($plugin->getWebSockets() as $ws){
                    $filename = $plugin->getPath() . "/websocket/$ws.php";
                    if(file_exists($filename))
                    {
                        include_once $filename;
                        $classname = "{$ws}_websocket";
                        if(class_exists($classname))
                        {

                            $router->addRoute("#^/$ws(\?.*)?$#i", new $classname($this->getApp()));
                        }
                    }
                }
            }

// Bind the server
            $server->bind();
            try{

                // Start the event loop

                $loop->run();
            }
            catch(Exception $ex)
            {
                \Dunp\Log::e($ex->getTraceAsString());
            }

            \Dunp\Log::d("Stopped at: " . date("d-m-Y H:i:s", time()));
        }
        else
        {
            \Dunp\Log::w("WebSocket server already running!");
        }

        return true;
    }
}