<?php

\Dunp\Plugin\PluginRegister::addPlugin("system_websocket");

class system_websocket_plugin extends Dunp\Plugin\Plugin
{
    /**
     *
     * @param Dunp\WebApp $app
     * @param type $path
     */
    public function __construct($app, $path) {
        parent::__construct($app, $path, "WebSocket Server", "dunpmusic");

        $this->addCliCommand("websocket");
    }
}
