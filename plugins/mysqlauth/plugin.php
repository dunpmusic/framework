<?php
\Dunp\Plugin\PluginRegister::addPlugin("mysql_auth");
class mysql_auth_plugin extends Dunp\Plugin\Plugin
{
    /**
     *
     * @param Dunp\WebApp $app
     * @param type
     */
    public function __construct($app, $path) {
        parent::__construct($app, $path, "MySQL Authorization Manager", "dunpmusic", "[logo.png]", "[plugin description]");
        $this->autoloader();
    }


}
