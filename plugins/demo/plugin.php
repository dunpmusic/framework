<?php
\Dunp\Plugin\PluginRegister::addPlugin("demo_web_app");

class demo_web_app_plugin extends Dunp\Plugin\Plugin
{
    public function __construct($app, $path) {
        parent::__construct($app, $path, "Simple Web app", "dunpmusic", "logo.png", "demo web app");
        $this->addPage("index");
    }
}