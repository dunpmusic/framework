# Plugins


Folder structure:
```
plugin_name
│   plugin.php
│
├───cli
│   │   command_line_scripts.php
│   │   ...
│
├───controller
│   │   page_name.js
│   │   ...
│
├───model
│   │   table_model.json
│   │   ...
│
├───page
│   │   page_name.php
│   │   ...
│
├───resources
│   │   (Some public static resources)
│   │   ...
│
├───src
│   │   (Source Implementations)
│   │   ...
│
└───view
    │   page_name.html
    │   ...
    
```